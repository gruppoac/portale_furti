/**
 * Created by antoniov on 07/12/2015.
 */
var gulp = require('gulp');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var env = process.env.GULP_ENV;

//JAVASCRIPT TASK: write one minified js file out of jquery.js, bootstrap.js and all of my custom js files
gulp.task('js', function () {
    return gulp.src([
            //'assets/jquery.session/jquery.session.js',
            //'assets/metisMenu/dist/metisMenu.js',
            //'assets/jquery-slimscroll/jquery.slimscroll.min.js',
            //'assets/pace/pace.min.js',
            //'assets/hinclude/hinclude.js',
            //old datatables
            //'app/Resources/assets/dataTables/js/jquery.dataTables.js',
            //'app/Resources/assets/dataTables/js/dataTables.bootstrap.js',
            //'app/Resources/assets/dataTables/js/dataTables.responsive.js',
            //'app/Resources/assets/dataTables/js/dataTables.tableTools.min.js',
            //new datatables
            //'assets/datatables.net/js/jquery.dataTables.js',
            //'assets/datatables.net-bs/js/dataTables.bootstrap.js',
            //'assets/datatables.net-buttons/js/dataTables.buttons.js',
            //'assets/datatables.net-buttons/js/buttons.html5.js',
            //'assets/datatables.net-buttons/js/buttons.print.js',
            //'assets/datatables.net-buttons-bs/js/buttons.bootstrap.js',
            //'assets/datatables.net-responsive/js/dataTables.responsive.js',
            //'assets/datatables.net-responsive-bs/js/responsive.bootstrap.js',
            //'assets/datatables.net-select/js/dataTables.select.js',

            //'assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
            //'assets/select2/dist/js/select2.full.min.js',
            //'assets/sweetalert/dist/sweetalert.min.js',
            //'assets/icheck/icheck.min.js',
            //'app/Resources/assets/js/**/*.js'
            'app/Resources/assets/javascripts.js'
            ])
        .pipe(concat('javascript.js'))
        .pipe(gulpif(env === 'prod', uglify()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/assets/js'));
});

//CSS TASK: write one minified css file out of bootstrap.less and all of my custom less files
gulp.task('css', function () {
    return gulp.src([
            //'app/Resources/assets/css/style.css',
            //'assets/metisMenu/dist/metisMenu.css',
            //'assets/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
            //'assets/select2/dist/css/select2.min.css',
            //'assets/animate.css/animate.css',
            //'assets/sweetalert/dist/sweetalert.css',
            //'app/Resources/assets/plugins/icheck/css/green.css',
            
            //new datatables
            //'assets/datatables.net-bs/css/dataTables.bootstrap.css',
            //'assets/datatables.net-buttons-bs/css/buttons.bootstrap.css',
            //'assets/datatables.net-responsive-bs/css/responsive.bootstrap.css',
            //'assets/datatables.net-select-bs/css/select.bootstrap.css',
            //old datatables
            //'app/Resources/assets/dataTables/css/dataTables.css',
            //'app/Resources/assets/dataTables/css/responsive.css',
            //'app/Resources/assets/dataTables/css/tableTools.min.css',
            'app/Resources/assets/style.css'

            //'app/Resources/assets/css/**/*.css'
        ])
        .pipe(concat('styles.css'))
        .pipe(gulpif(env === 'prod', uglifycss()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/assets/css'));
});

//IMAGE TASK: Just pipe images from project folder to public web folder
gulp.task('img', function() {
    return gulp.src([
            'app/Resources/assets/css/plugins/icheck/img/*.*',
            'app/Resources/assets/img/*.*',
            'app/Resources/assets/img/**/*.*'])
        .pipe(gulp.dest('web/assets/img'));
});

// Fonts
gulp.task('fonts', function() {
    return gulp.src([
            'assets/bootstrap/fonts/glyphicons-halflings-regular.*',
            'assets/components-font-awesome/fonts/fontawesome-webfont.*'
        ])
        .pipe(gulp.dest('web/assets/fonts'));
});

// Extra file
gulp.task('extra', function() {
    return gulp.src([
            'app/Resources/assets/extra/*.*',
            'assets/dataTables/swf/copy_csv_xls_pdf.swf',
            'app/Resources/public/extra/**/*.*'])
        .pipe(gulp.dest('web/assets/extra'));
});

//define executable tasks when running "gulp" command
gulp.task('default', ['js', 'css', 'img']);
//'fonts', 'extra'