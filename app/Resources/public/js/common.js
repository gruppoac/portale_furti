/**
 * Created by antoniov on 17/12/2015.
 */
function getSuccessNotification(message) {

    var $alert = '';

    $alert += '<div class="alert alert-success alert-dismissable">';
    $alert += '     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
    $alert += '' + message;
    $alert += '</div>';

    // Show notification
    $("#notifications").empty().append($alert);
}

function getWarningNotification(message) {

    var $alert = '';

    $alert += '<div class="alert alert-warning alert-dismissable">';
    $alert += '     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
    $alert += '' + message;
    $alert += '</div>';

    // Show notification
    $("#notifications").empty().append($alert);
}

function appendSuccessNotification(message) {

    var $alert = '';

    $alert += '<div class="alert alert-success alert-dismissable">';
    $alert += '     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
    $alert += '' + message;
    $alert += '</div>';

    // Show notification
    $("#notifications").append($alert);
}

function getErrorNotification(message) {

    var $alert = '';

    $alert += '<div class="alert alert-danger alert-dismissable">';
    $alert += '     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
    $alert += '' + message;
    $alert += '</div>';

    // Show notification
    $("#notifications").empty().append($alert);
}

function appendErrorNotification(message) {

    var $alert = '';

    $alert += '<div class="alert alert-danger alert-dismissable">';
    $alert += '     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
    $alert += '' + message;
    $alert += '</div>';

    // Show notification
    $("#notifications").append($alert);
}

// Send form via ajax
function postForm( $form, callback ){
    /*
     * Get all form values
     */
    var values = {};
    $.each( $form.serializeArray(), function(i, field) {
        values[field.name] = field.value;
    });

    /*
     * Throw the form values to the server!
     */
    $.ajax({
        type        : $form.attr( 'method' ),
        url         : $form.attr( 'action' ),
        data        : values,
        success     : function(data) {
            callback( data );
        }
    });
}

// Update data in table
function updateTable(table_id, table_config, anomalies) {
    var table = $(table_id),
        dtable = table.DataTable(table_config);

    dtable.clear();
    dtable.rows.add(anomalies);
    dtable.draw();
}

function removeRowTable(table_id) {
    var table = $(table_id).DataTable();

    table.row('.selected').remove().draw( false );
}

// Get Json array
function getJSonObject(value) {
    return $.parseJSON(value.replace(/&quot;/ig, '"'));
}

