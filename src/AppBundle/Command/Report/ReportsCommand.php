<?php

namespace AppBundle\Command\Report;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;

/**
 * Script che crea il file CSV con tutte le pratiche a seconda dei diversi stati
 * bin/console steals:dashboard-report --status=3 -vv
 */
class ReportsCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('steals:dashboard-report')
            ->setDescription('Pratiche in stato di attesa')
            ->addOption('status', null, InputOption::VALUE_REQUIRED, 'stato');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $status = $input->getOption("status");
        $entities = $this->getRecords($status);
        $label = $this->labelStatus($status);
        $csv = $this->getContainer()->getParameter("csv_directory").'/reports/report_' . $label . '.csv';
        $fp = fopen($csv, 'w');

        $row = array(
            'isaStealCarPlate'              => 'Targa',
            'isaStealCarModel'              => 'Modello',
            'isaStealDeviceSn'              => 'Seriale',
            'isaStealType'                  => 'tipo',
            'isaStealInsertDate'            => 'Data creazione',
            'isaStealDate'                  => 'Data presunta furto/denuncia',
            'isa_steal_update_date'         => 'Data chiusura evento',
            'isaStealHolderName'            => 'Nome e Cognome Cliente',
            'isaStealCarInsuranceNumber'    => 'Polizza',
            'isaStealLat'                   => 'Latitudine',
            'isaStealLong'                  => 'Longitudine',
            'isaStealUser'                  => 'Utente'
        );
        fputcsv($fp, $row);
         
        foreach($entities as $entity) {
                $row = array(
                    'isaStealCarPlate'              => $entity->getIsaStealCarPlate(),
                    'isaStealCarModel'              => $entity->getIsaStealCarModel(),
                    'isaStealDeviceSn'              => $entity->getIsaStealDeviceSn(),
                    'isaStealType'                  => $entity->getIsaStealType(),
                    'isaStealInsertDate'            => ($entity->getIsaStealInsertDate()) ? $entity->getIsaStealInsertDate()->format('Y-m-d H:i:s') : null,
                    'isaStealDate'                  => ($entity->getIsaStealDate()) ? $entity->getIsaStealDate()->format('Y-m-d H:i:s') : null,
                    'isa_steal_update_date'         => ($entity->getIsaStealUpdateDate()) ? $entity->getIsaStealUpdateDate()->format('Y-m-d H:i:s') : null,
                    'isaStealHolderName'            => $entity->getIsaStealHolderName(),
                    'isaStealCarInsuranceNumber'    => $entity->getIsaStealCarInsuranceNumber(),
                    'isaStealLat'                   => $entity->getIsaStealLat(),
                    'isaStealLong'                  => $entity->getIsaStealLong(),
                    'isaStealUser'                  => $entity->getIsaStealUser()
                );    
                fputcsv($fp, $row);

            $output->writeln($entity->getIsaStealCarPlate());
            $this->getOm()->clear();
        }
        
        fclose($fp);
        $output->writeln("FILE ESPORTATO");
    }
    
    protected function getRecords($status) {
        $qb = $this->getOm()->getRepository("AppBundle:IsaSteals")->findByIsaStealStatus($status);
        return $qb;
    }
    
    protected function getOm() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    protected static function labelStatus($status)
    {
        switch($status) {
            case 1:
                return "working";
            case 2:
                return "closed";
            case 3:
                return "pending";
            default:
                return "default";
        }
    }

}
