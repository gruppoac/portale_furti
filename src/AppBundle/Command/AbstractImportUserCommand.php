<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Mailer\DmisaMailer;
use FOS\UserBundle\Model\UserManagerInterface;
use AppBundle\Entity\User;
use Psr\Log\LoggerInterface;
use Exception;

abstract class AbstractImportUserCommand extends ContainerAwareCommand {
   

    /**
     * Creazione utente
     * @param string $username
     * @param string $email
     * @param array $roles
     * @param string $password
     * @return User
     */
    protected function createUser($username, $email, $userType, $roles = array(), $password = null, $flush = false) {
        
        $user = $this->getUserManager()->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setEnabled(true);
        $user->setUserType($userType);
        
        
        //add password
        if (!$password) {
            $password = $this->generatePassword();
        }
        $user->setPlainPassword($password);
        
        $user->setRecover($password);
        $user->setEmailSent(false);
        
        
        //add roles
        foreach ($roles as $role) {
            $user->addRole($role);
        }
        //save user
        $this->saveUser($user, $flush);
        
        $this->getLogger()->info($user->getUsername() . " creato correttamente");
        return $user;
    }    

    protected function checkUser($username) {
        return $this->getUserManager()->findUserByUsername($username);
    }

    protected function generatePassword($length = 8) {
        $tokenGenerator = $this->getContainer()->get('fos_user.util.token_generator');
        return substr($tokenGenerator->generateToken(), 0, $length); // 8 chars        
    }

    /**
     * Valida e salva l'officina
     * @param User $user
     * @return User
     */
    protected function saveUser(User $user, $flush = true) {
        $this->validate($user);
        $this->getUserManager()->updateUser($user, $flush);
        return $user;
    }

    /**
     * Validazione utente
     * @param type $entity
     * @return boolean
     * @throws \Exception
     */
    protected function validate($entity, $groups = null) {
        $errors = $this->getValidator()->validate($entity, null, $groups);
        foreach ($errors as $error) {
            throw new Exception($error->getMessage());
        }
        return true;
    }

    /**
     * @return ObjectManager
     */
    protected function getOm() {
        return $this->getContainer()->get('doctrine')->getManager();
    }
    
    /**
     * @return LoggerInterface
     */
    protected function getLogger() {
        return $this->getContainer()->get('logger');
    }    

    /**
     * @return ValidatorInterface
     */
    protected function getValidator() {
        return $this->getContainer()->get('validator');
    }

    /**
     * @return UserManagerInterface
     */
    protected function getUserManager() {
        $userManager = $this->getContainer()->get('fos_user.user_manager');
        return $userManager;
    }

    /**
     * @return DmisaMailer
     */
    protected function getMailer() {
        return $this->getContainer()->get('dmisa_mailer');
    }

}
