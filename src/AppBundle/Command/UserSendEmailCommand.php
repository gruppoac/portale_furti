<?php

namespace AppBundle\Command;

//use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\User;

/**
 * Controlla tutti gli utenti con emailSent a 0 e invia le email 
 */
class UserSendEmailCommand extends AbstractImportUserCommand {

    
    protected function configure() {
        $this
            ->setName('dms:user-send-email')    //aggiungere --env prod <-- altrimenti manda email da ambiente di sviluppo
            ->setDescription('Invia email agli utenti')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        
        $env = $this->getContainer()->get('kernel')->getEnvironment();
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost( $this->getContainer()->getParameter('host') );
        $context->setScheme( $this->getContainer()->getParameter('scheme') );
        
        $array = $this->getContainer()->get('doctrine')->getManager()->createQueryBuilder()
                ->select('u.id, u.email, u.recover')
                ->from('AppBundle:User', 'u')
                ->where('u.emailSent = 0')
                ->andWhere('u.userType = :type')->setParameter('type', User::WORKSHOP)
                ->getQuery()->getArrayResult();

        foreach($array as $row ) {
            $this->parseRow($row);                
        }        
    }
        
    protected function parseRow($row) {
        try {         
            $user = $this->getUserManager()->findUserByEmail($row['email']);
            if($user && $row['recover']) {
                
                $user->setPlainPassword($row['recover']);
                $this->getMailer()->sendImportUserEmailMessage($user);
                $this->getLogger()->info('invio email: '.$user->getUsername()." ".$user->getPlainPassword());

                //$user->setRecover(null);
                $user->setEmailSent(true);
                $this->getUserManager()->updateUser($user);
            }
        }
        catch (\Exception $e) {
            $this->getLogger()->alert($e->getMessage());
        }            
    }
}
