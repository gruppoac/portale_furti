<?php 

namespace AppBundle\Exceptions;

use Exception;

/**
 * Questa eccezione viene chiamata nel caso in cui la pratica di furto non può essere preso in carico perchè già in lavorazione
 * da un altro operatore
 */
class AccessStealException extends Exception {
    
    
    public function __construct ($message = "", $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}
