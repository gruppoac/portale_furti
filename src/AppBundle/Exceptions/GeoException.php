<?php 

namespace AppBundle\Exceptions;

use Exception;

/**
 * Questa eccezione viene chiamata nel caso in cui la geolocalizzazione fallisce
 */
class GeoException extends Exception {
    
    
    public function __construct ($message = "", $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}
