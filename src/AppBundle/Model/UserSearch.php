<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\UserInterface;

/**
 */
class UserSearch 
{
    /**
     * @Assert\NotBlank
     * @var UserInterface
     */
    protected $user;

    public function getUser() {
        return $this->user;
    }

    public function setUser(UserInterface $user) {
        $this->user = $user;
    }


}