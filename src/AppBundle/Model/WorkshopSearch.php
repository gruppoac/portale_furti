<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;


/**
 */
class WorkshopSearch 
{
    /**
     * @Assert\NotBlank
     */
    protected $workshop;

    public function getWorkshop() {
        return $this->workshop;
    }

    public function setWorkshop($workshop) {
        $this->workshop = $workshop;
    }


}