<?php

namespace AppBundle\Model\IsaSteal;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\IsaStealsTypes;
use AppBundle\Entity\Cars;
use DateTime;


class CarReportingModel
{
    /**
     * @var string
     * @Assert\NotBlank(message="Targa non trovata")
     */
    protected $plate;

    /**
     * @var IsaStealsTypes
     * @Assert\NotBlank(message="Nessun tipo di furto selezionato")
     * Assert\Choice(choices = {1, 2}, message = "tipo di furto selezionato non valido.")
     */
    protected $tipo;

    /**
     * @var string
     */
    protected $complaint;

    /**
     * @Assert\DateTime
     * @var DateTime
     */
    protected $date;

    /**
     * @return string
     */
    public function getPlate()
    {
        return $this->plate;
    }

    /**
     * @param string $plate
     */
    public function setPlate($plate)
    {
        $this->plate = $plate;
    }

    /**
     * @return IsaStealsTypes
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param IsaStealsTypes $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return string
     */
    public function getComplaint()
    {
        return $this->complaint;
    }

    /**
     * @param string $complaint
     */
    public function setComplaint($complaint)
    {
        $this->complaint = $complaint;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}