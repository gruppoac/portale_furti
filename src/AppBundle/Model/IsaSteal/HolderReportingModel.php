<?php

namespace AppBundle\Model\IsaSteal;

use AppBundle\Entity\IsaStealsTypes;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;


class HolderReportingModel
{
    /**
     * @var string
     * @Assert\NotBlank(message="Nome non trovato")
     */
    protected $name;

    /**
     * @var string
     * @Assert\NotBlank(message="Cognome non trovato")
     */
    protected $surname;

    /**
     * @var string
     * @Assert\NotBlank(message="Targa non trovata")
     */
    protected $plate;

    /**
     * @var IsaStealsTypes
     * @Assert\NotBlank(message="Nessun tipo di furto selezionato")
     */
    protected $tipo;

    /**
     * @var string
     */
    protected $complaint;

    /**
     * @Assert\DateTime
     * @var DateTime
     */
    protected $date;

    public function __construct() {
//        $this->date = new DateTime;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getPlate()
    {
        return $this->plate;
    }

    /**
     * @param string $plate
     */
    public function setPlate($plate)
    {
        $this->plate = $plate;
    }

    /**
     * @return IsaStealsTypes
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param IsaStealsTypes $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getComplaint()
    {
        return $this->complaint;
    }

    /**
     * @param mixed $complaint
     */
    public function setComplaint($complaint)
    {
        $this->complaint = $complaint;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}