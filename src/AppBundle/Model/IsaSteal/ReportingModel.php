<?php

namespace AppBundle\Model\IsaSteal;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\IsaStealsTypes;
use DateTime;


class ReportingModel
{
    /**
     * @var IsaStealsTypes
     * @Assert\NotBlank(message="Nessun tipo di furto selezionato")
     * Assert\Choice(choices = {1, 2}, message = "tipo di furto selezionato non valido.")
     */
    protected $tipo;
    
    /**
     * @Assert\DateTime
     * @var DateTime
     */
    protected $date;


    /**
     * @return IsaStealsTypes
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param IsaStealsTypes $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}