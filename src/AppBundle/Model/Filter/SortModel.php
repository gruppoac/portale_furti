<?php

namespace AppBundle\Model\Filter;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Oggetto per la gestione degli ordinamenti
 */
class SortModel {

    const ASC = "a";
    const DESC = "d";
    
    /**
     * @var string 
     */
    protected $order;
    
    /**
     * @var string 
     * @Assert\Choice(choices={"a", "d"})
     */
    protected $type = self::ASC;

    public function __construct() {
        
    }
    
    public function getOrder() {
        return $this->order;
    }

    public function getType() {
        return $this->type;
    }

    public function setOrder($order) {
        $this->order = $order;
        return $this;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }


    
}

