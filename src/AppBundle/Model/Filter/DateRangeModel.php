<?php

namespace AppBundle\Model\Filter;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Oggetto per la gestione del range date
 */
class DateRangeModel {

    /**
     * @var string
     */
    protected $start;

    /**
     * @var string
     */
    protected $end;
    

    public function __construct() {
        
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setStart($start)
    {
        $this->start = $start;
        return $this;
    }

    public function setEnd($end)
    {
        $this->end = $end;
        return $this;
    }



    
}

