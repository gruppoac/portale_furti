<?php

namespace AppBundle\Model\Filter;

/**
 * Classe per la gestione della paginazione,
 * in automatico crea il "widget" per la selezione della pagina 
 */
class Pagination {
    
    protected $page;
    protected $limit;
    protected $tot;
    
    protected $prev;
    protected $next;
    protected $last;
    
    protected $range = 3;

    protected $pages = array();
    
    public function __construct($page, $limit, $tot) {
        $this->page = $page;
        $this->limit = $limit;
        $this->tot = $tot;
        
        $this->last = ceil($tot / $limit);
        
        $this->prev = $page > 0 ? ($this->page - 1) : 0;
        $this->next = $page < $this->last ? ($this->page + 1) : $this->last;

        //crea range paginazione;
        $min = $page - $this->range;
        $delta = 0;
        if($min < 1 ) {
            $delta = abs($min);
            $min = 1;
        }
        $max = $page + $this->range + $delta;
        if($max >= $this->last ) {
            $max = $this->last;
        }
        
        for($i = $min; $i<=$max; $i++) {
            $this->pages[] = $i; 
        }        
    }

    public function getPage() {
        return $this->page;
    }

    public function getLimit() {
        return $this->limit;
    }

    public function getTot() {
        return $this->tot;
    }

    public function getPrev() {
        return $this->prev;
    }

    public function getNext() {
        return $this->next;
    }

    public function getRange() {
        return $this->range;
    }

    public function getPages() {
        return $this->pages;
    }

    public function setPage($page) {
        $this->page = $page;
        return $this;
    }

    public function setLimit($limit) {
        $this->limit = $limit;
        return $this;
    }

    public function setTot($tot) {
        $this->tot = $tot;
        return $this;
    }

    public function setPrev($prev) {
        $this->prev = $prev;
        return $this;
    }

    public function setNext($next) {
        $this->next = $next;
        return $this;
    }

    public function setRange($range) {
        $this->range = $range;
        return $this;
    }

    public function setPages($pages) {
        $this->pages = $pages;
        return $this;
    }


    public function getLast() {
        return $this->last;
    }

    public function setLast($last) {
        $this->last = $last;
        return $this;
    }

    
    
}

