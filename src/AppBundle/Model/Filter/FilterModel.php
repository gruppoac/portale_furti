<?php

namespace AppBundle\Model\Filter;

use AppBundle\Model\Filter\SortModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Modello di default per i filtri di ricerca, questa classe può essere estesa
 * per filtri di ricerca più specifici
 */
class FilterModel {
    
    /**
     * @var SortModel 
     * @Assert\Valid
     */
    protected $sort;

    /**
     * @var integer 
     * @Assert\NotBlank
     */
    protected $page = 1;

    /**
     * @var integer 
     * @Assert\NotBlank
     * @Assert\Range(
     *      min = 5,
     *      max = 100,
     * )
     */
    protected $limit = 100;

    /**
     * @var string
     */
    protected $query;

    /**
     * @var DateRangeModel
     * @Assert\Valid
     */
    protected $dateRange;

    
    public function __construct() {
        $this->sort = new SortModel;
        $this->dateRange = new DateRangeModel();
    }


    public function getSort() {
        return $this->sort;
    }

    public function getPage() {
        return (int)$this->page;
    }

    public function getLimit() {
        return (int)$this->limit;
    }

    public function getQuery() {
        return $this->query;
    }

    public function getDateRange()
    {
        return $this->dateRange;
    }

    public function setSort(SortModel $sort) {
        $this->sort = $sort;
        return $this;
    }

    public function setPage($page) {
        $this->page = $page;
        return $this;
    }

    public function setLimit($limit) {
        $this->limit = $limit;
        return $this;
    }

    public function setQuery($query) {
        $this->query = $query;
        return $this;
    }

    public function setDateRange($dateRange)
    {
        $this->dateRange = $dateRange;
        return $this;
    }



    
}

