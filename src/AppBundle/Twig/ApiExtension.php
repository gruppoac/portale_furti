<?php

namespace AppBundle\Twig;

use AppBundle\Services\ApiManager;
use Doctrine\Common\Persistence\ObjectManager;

class ApiExtension extends \Twig_Extension {

    /**
     * @var ApiManager 
     */
    protected $api;

    /**
     * @var ObjectManager 
     */
    protected $om;

    /**
     * @var array
     */
    protected $branches;

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('workshopBranch', array($this, 'getWorkshopBranch')),
            new \Twig_SimpleFunction('carByDeviceId', array($this, 'getCarByDeviceId')),
            new \Twig_SimpleFunction('deviceById', array($this, 'getDeviceById')),
        );
    }

    public function getDeviceById($deviceId) {
        $car = $this->om->getRepository("AppBundle:Devices")->createQueryBuilder('d')
                        ->andWhere('d.deviceId = :deviceId')->setParameter('deviceId', $deviceId)
                        ->getQuery()->getOneOrNullResult();
        return $car;
    }

    public function getWorkshopBranch($code) {
        if(!$code ) {
            return null;
        }
        try {
            return $this->api->cacheBranch($code);
        } catch (\Exception $e) {
            return null;
        }
    }
    

    public function getCarByDeviceId($deviceId) {
        $car = $this->om->getRepository("AppBundle:Cars")->createQueryBuilder('c')
                        ->andWhere('c.carDeviceFk = :deviceId')->setParameter('deviceId', $deviceId)
                        ->getQuery()->getOneOrNullResult();
        return $car;
    }

    public function getName() {
        return 'api_extension';
    }

    public function setApi(ApiManager $api) {
        $this->api = $api;
    }

    public function setObjectManager(ObjectManager $om) {
        $this->om = $om;
    }

}
