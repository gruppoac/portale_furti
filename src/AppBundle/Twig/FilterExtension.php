<?php

namespace AppBundle\Twig;

use Twig_Environment;
use AppBundle\Model\Filter\Pagination;

/**
 * Funzioni twig per la gestione della paginazione
 */
class FilterExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('pagination', array($this, 'pagination'), array('is_safe' => array('html'), 'needs_environment' => true) ),
        );
    }

    /**
     * Crea la paginazione renderizzando una vista twig
     * @param Twig_Environment $env 
     * @param integer $page
     * @param integer $limit
     * @param integer $tot
     * @return string
     */
    public function pagination(\Twig_Environment $env, $page, $limit, $tot) {

        $pager = new Pagination($page, $limit, $tot);
        return $env->render("form/filter/pagination.html.twig", array(
            'pager' => $pager
        ));
    }

    public function getName()
    {
        return 'filter_extension';
    }
}
