<?php

namespace AppBundle\Twig;
use AppBundle\Entity\IsaSteals;
use AppBundle\Helper\Blackbox;
use AppBundle\Helper\Date;
use AppBundle\Entity\IsaEvents;
use AppBundle\Helper\SerializerHelper;
use AppBundle\Helper\UserHelper;
use AppBundle\Helper\LabelStatus;

class DmisaExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('data', array($this, 'data')),
            new \Twig_SimpleFilter('dataOra', array($this, 'dataOra')),
            new \Twig_SimpleFilter('dataOraGMT', array($this, 'dataOraGMT')),
            new \Twig_SimpleFilter('deviceStatus', array($this, 'deviceStatus')),
            new \Twig_SimpleFilter('carStatus', array($this, 'carStatus')),
            new \Twig_SimpleFilter('eventStatus', array($this, 'eventStatus')),
            new \Twig_SimpleFilter('stealStatus', array($this, 'stealStatus')),
            new \Twig_SimpleFilter('stealTracking', array($this, 'stealTracking')),
            new \Twig_SimpleFilter('engineStatus', array($this, 'engineStatus')),
            new \Twig_SimpleFilter('userType', array($this, 'userType')),
            new \Twig_SimpleFilter('importStatus', array($this, 'importStatus')),
            new \Twig_SimpleFilter('unserialize', array($this, 'unserialize'))
        );
    }
    
    public function userType($code) {
        return UserHelper::getUserType($code);
    }
    
    public function data($data, $format = "d/m/Y")
    {
        return Date::gmtData($data, $format);
    }

    public function dataOra($data, $format = "d/m/Y H:m:s")
    {
        return Date::gmtData($data, $format);        
    }

    public function dataOraGMT($data, $format = "d/m/Y H:m:s", $gmt = 'GMT')
    {
        return Date::gmtDataTime($data, $format, $gmt);
    }

    public function eventStatus($status)
    {
        return IsaEvents::isaEventStatus($status);
    }

    public function stealStatus($status)
    {
        return IsaSteals::isaStealLabelStatus($status);
    }

    public function stealTracking($status)
    {
        return IsaSteals::isaStealLabelTracking($status);
    }

    public function engineStatus($status)
    {
        return LabelStatus::engineStatus($status);
    }

    public function deviceStatus($status)
    {
        return LabelStatus::getDeviceStatus($status);

    }     
    
    public function carStatus($status)
    {
        return LabelStatus::getCarStatus($status);

    }

    public function unserialize($str) {
        return SerializerHelper::unserialize($str);
    }

    public function getName()
    {
        return 'dmisa_extension';
    }
}