<?php

namespace AppBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class MailerManager
{
    protected $mailer;
    protected $router;
    protected $twig;
    protected $parameters;
    protected $from;
    protected $name;

    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, \Twig_Environment $twig, $from, $name = null)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
        $this->from = $from;
        $this->name = $name;   
    }
    
    /**
     * Invio email da script di cron, nella email verranno spediti nome utente e passowrd per accedere
     * @param UserInterface $user
     */
    public function sendImportUserEmailMessage(UserInterface $user)
    {        
        $template = "mailer/importUser.html.twig";
        $context = array(
            'user' => $user,
            'url'  => $this->router->generate('homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL)
        );
        
        $email = $user->getEmail();
        $this->sendMessage($template, $context, $this->from, $email );
    }
    
    /**
     * @param string $templateName
     * @param array  $context
     * @param string $fromEmail
     * @param string $toEmail
     */
    protected function sendMessage($templateName, $context, $fromEmail, $toEmail)
    {
        $context = $this->twig->mergeGlobals($context);
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $this->mailer->send($message);
    }    

}
