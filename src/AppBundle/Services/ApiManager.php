<?php

namespace AppBundle\Services;

use GuzzleHttp\Client;
use Doctrine\Common\Cache\Cache;

class ApiManager
{
    protected $apiKey;
    protected $host;
    protected $client;
    protected $path = '/api/v1/';
    protected $lifetime = 3600;
    
    /**
     * @var Cache 
     */
    protected $cache;
    
    public function __construct($host, $path, $apiKey, Cache $cache) {
        $this->apiKey = $apiKey;
        $this->host = $host;
        $this->path = $path;
        $this->cache = $cache;
        
        $this->client = new Client(
                array('base_uri' => $this->host,
                    'headers' => array(
                        'X-API-KEY' => $this->apiKey
                    )
                )
        );
    }

    
    public function getApi($uri, $querystring = array() ) {
        
        $response = $this->client->get($this->path.$uri, array(
            'query' => $querystring
        ));
        return json_decode( (string)$response->getBody(), true);
    }
    
    
    public function getWorkshopBranch($code) {
        return $this->getApi('workshops/branch/'.$code.'/info');
    }
    
    public function getWorkshopBranches(\DateTime $update = null) {
        $params = array();
        if($update) {
            $params['update'] = $update->format('Y-m-d H:i:s');
        }
        return $this->getApi('workshops/branches/all', $params);
    }
    
    public function searchWorkshopBranches($query) {
        $params = array();
        $params['query'] = $query;
        return $this->getApi('workshops/branches/all', $params);
    }    
    
    public function searchBranches($params = array() ) {

        return $this->getApi('workshops/branches/all', $params);
    }    
    
    public function cacheBranches() {
        $key = 'api_branches_all';
        if($this->cache->contains($key)) {
            return $this->cache->fetch($key);
        }
        
        $json = $this->searchBranches( array() );
        $this->cache->save($key, $json['list'], $this->lifetime );
        return $json['list'];
    }
    
    public function cacheBranch($code) {
        $key = 'api_branches_'.$code;
        if($this->cache->contains($key)) {
            return $this->cache->fetch($key);
        }
        
        $json = $this->getWorkshopBranch( $code );
        $this->cache->save($key, $json, $this->lifetime );
        return $json;
    }

    public function getPolizzaById($code) {
        return $this->getApi('polizze/'.$code.'/info');
    }

    public function getPolizzaByVoucher($voucher, $params = array()) {
        return $this->getApi('vouchers/' . $voucher . '/polizze', $params);
    }
}