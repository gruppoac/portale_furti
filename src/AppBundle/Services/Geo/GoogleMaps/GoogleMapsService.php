<?php

namespace AppBundle\Services\Geo\GoogleMaps;

//use Ivory\HttpAdapter as Adapters;
use AppBundle\Entity\Geo;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Geocoder\Provider\GoogleMapsProvider;
use Geocoder\Geocoder as GeocoderInterface;
use Doctrine\Common\Cache\Cache;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Psr\Log\LoggerInterface;
use AppBundle\Exceptions\GeoException;

class GoogleMapsService {

    /** @var string */
    protected $clientId;
    /** @var string*/
    protected $clientSecret;
    /** @var GeocoderInterface */
    protected $geocoder;
    /** @var Cache */
    protected $cache;
    /** @var ValidatorInterface */
    protected $validator;
    /** @var LoggerInterface */
    protected $logger;

    public function getClientId() {
        return $this->clientId;
    }

    public function getClientSecret() {
        return $this->clientSecret;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    public function setClientSecret($clientSecret) {
        $this->clientSecret = $clientSecret;
    }

    public function __construct($apiKey = null) {
        //$adapter = new Adapters\Guzzle6HttpAdapter();
        $adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter();
        $this->geocoder = new GoogleMapsProvider($adapter, 'it_IT', true, $apiKey);
    }

    /**
     * Salvo in cache il risultato
     * @param type $value
     */
    public function geocode($value) {
        $key = md5($value);
        if($this->cache->contains($key)) {
            return $this->cache->fetch($key);
        }
        try {
            $geo = $this->geocoder->geocode($value);
        }
        catch (\Exception $e ) {
            throw new GeoException($e->getMessage() );
        }
        $data = $geo->first();
        $this->cache->save($key, $data->toArray() );
        return $data->toArray();
    }

    public function reverse($latitude, $longitude) {
        $key = md5($latitude . ';' . $longitude);
        if($this->cache->contains($key)) {
            return $this->cache->fetch($key);
        }
        try {
            $geo = $this->geocoder->getReversedData(array($latitude, $longitude));
        }
        catch (\Exception $e ) {
            throw new GeoException($e->getMessage() );
        }
        $data = $geo[0];
        $this->cache->save( $key, $data );
        return $data;
    }

    /**
     * 
     * @param string $query
     * @return Geo
     */
    public function createGeoFromQuery($query) {

        $geocode = $this->geocode($query);

        $geo = new Geo;
        $geo->setQuery($query);

        if($geocode['countryCode'] != "IT") {
            throw new GeoException($query.": Non mi risulta in italia!");
        }


        $geo->setIndirizzo($geocode['streetName']. " " . $geocode['streetNumber']);
        $geo->setCap($geocode['postalCode']);

        $levels = $geocode['adminLevels'];

        if (!isset($levels[1] ) ) {
            throw new GeoException($query.": Regione non trovata");
        }
        if (!isset($levels[2] ) ) {
            throw new GeoException($query.": Provincia non trovata");
        }
        if (!isset($levels[3] ) ) {
            throw new GeoException($query.": Comune non trovato");
        }
        $geo->setComune($levels[3]['name']);

        $geo->setProvincia($levels[2]['name']);
        $geo->setProvinciaCode($levels[2]['code']);
        
        $geo->setRegione($levels[1]['name']);

        $point = new Point($geocode['longitude'], $geocode['latitude'] );
        $geo->setPoint($point);

        $this->validate($geo);
        return $geo;

    }

    /**
     * Validazione utente
     * @param type $entity
     * @return boolean
     * @throws \Exception
     */
    protected function validate($entity, $groups = null) {
        $errors = $this->validator->validate($entity, null, $groups);
        foreach ($errors as $error) {
            $this->logger->warning("geo: ". $error->getMessage() );
            throw new GeoException( $error->getMessage() );
        }
        return true;
    }    

    /**
     * @param Cache $cache
     */
    public function setCache(Cache $cache) {
        $this->cache = $cache;
    }
    
    public function setValidator(ValidatorInterface $validator) {
        $this->validator = $validator;
        return $this;
    }

    public function setLogger(LoggerInterface $logger) {
        $this->logger = $logger;
        return $this;
    }

    
}
