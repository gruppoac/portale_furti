<?php

namespace AppBundle\Services;

use Geocoder\Geocoder;
use Exception;

class GeocoderManager
{
    protected $clientId;
    protected $clientSecret;
    protected $geocoder;
    
    
    public function getClientId() {
        return $this->clientId;
    }

    public function getClientSecret() {
        return $this->clientSecret;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    public function setClientSecret($clientSecret) {
        $this->clientSecret = $clientSecret;
    }

    public function __construct($apiKey = null) {
        $adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter();
        $provider = new \Geocoder\Provider\GoogleMapsProvider($adapter, 'it_IT', 'Italy', true, $apiKey);
    
        $this->geocoder = new Geocoder();
        $this->geocoder->registerProvider($provider);
    }

    
    public function geocode($value) {
        return $this->geocoder->geocode($value);
    }
    
    public function reverse($latitude, $longitude) {
        return $this->geocoder->reverse($latitude, $longitude);
    }
    
    public function getAddress($lat, $lon) {
        //possiamo usare direttamente le API 
        try {
            $address_obj = $this->reverse($lat, $lon);
            // Localization address
            $loc_address = $address_obj->getStreetName() . ' ' . $address_obj->getStreetNumber() . ' - ' . $address_obj->getZipcode() . ' ' . $address_obj->getCity();
            return $loc_address;
        }
        catch (Exception $e) {
            return "address not found: ".$e->getMessage();
        }
        return null;
    }
        
    
    
}