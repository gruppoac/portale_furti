<?php

namespace AppBundle\Services;

use GuzzleHttp\Client;
use Doctrine\Common\Cache\Cache;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Config\Definition\Exception\Exception;

class ApiGatewayManager
{
    protected $apiKey;
    protected $host;
    protected $client;
    protected $path = '/';
    protected $lifetime = 3600;
    
    /**
     * @var Cache 
     */
    protected $cache;
    
    public function __construct($host) {
        $this->host = $host;
        
        $this->client = new Client(
                array('base_uri' => $this->host)
        );
    }
    
    public function getApi($uri, $querystring = array() ) {
        try {
            $response = $this->client->get($this->path.$uri, array(
                'query' => $querystring
            ));
            return json_decode( (string)$response->getBody(), true);
        }
        catch (ClientException $exception) {
            return $exception->getCode();
        }
    }

    public function postApi($uri, $params = array(), $header = null) {
        try {
            if ($header) {
                $response = $this->client->post($this->path.$uri, ['json' => $params, 'headers' => $header]);
            } else {
                $response = $this->client->post($this->path.$uri, ['json' => $params]);
            }
            return json_decode( (string)$response->getBody(), true);
        }
        catch (ClientException $exception) {
            return $exception->getCode();
        }
    }

    public function postBodyApi($uri, $params = array(), $header = null) {
        try {
            if ($header) {
                //$response = $this->client->post($this->path.$uri, ['body' => $params, 'headers' => $header]);
                $response = $this->client->post($this->path.$uri, array(
                    'multipart' => array('serial' => $params['serial'], 'command' => $params['command'])));
            } else {
                //$response = $this->client->post($this->path.$uri, ['body' => $params]);
                $response = $this->client->post($this->path.$uri, array(
                    'multipart' => array('serial' => $params['serial'], 'command' => $params['command'])));
            }
            return json_decode( (string)$response->getBody(), true);
        }
        catch (ClientException $exception) {
            return $exception->getCode();
        }
    }
    
    public function getVoucherByHolder($name, $surname) {
        return $this->getApi('gatewayEventsApi/voucher/'.$name.'/'.$surname);
    }

    public function getVoucherByPlate($plate) {
        return $this->getApi('gatewayEventsApi/voucher/targa/'.$plate);
    }

    public function getVoucher($voucher) {
        return $this->getApi('gatewayEventsApi/voucher/'.$voucher);
    }

    public function getLocalizationEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/localization/tracking/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getLastLocalizationEvent($serial, $params = array()) {
        $url = 'gatewayEventsApi/localization/'.$serial;
        return $this->getApi($url, $params);
    }

    public function getCountLocalizationEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/localization/tracking/count/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getTamperEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/tamper/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getCountTamperEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/tamper/count/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getEngineEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/localization/engine/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getCountEngineEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/localization/engine/count/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getPowerEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/engine/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getCountPowerEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/engine/count/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getMovingEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/engineOff/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function setCommand($params) {
        return $this->postApi('gatewayApi/command/query', $params);
    }

    public function getLastCommands($serial, $command, $limit = null, $params = array()) {
        $url = 'gatewayApi/command/'.$serial.'/'.$command;
        (null !== $limit) ? $url = $url . '/' . $limit : null;
        return $this->getApi($url, $params);
    }

    public function getCommandById($id, $params = array()) {
        return $this->getApi('gatewayApi/command/'.$id, $params);
    }

    public function getSosEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/sos/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getCountSosEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/sos/count/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getCrashEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/crash/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function getCountCrashEvents($serial, $start, $end = null, $params = array()) {
        $url = 'gatewayEventsApi/event/crash/count/'.$serial.'/'.$start;
        (null !== $end) ? $url = $url . '/' . $end : null;
        return $this->getApi($url, $params);
    }

    public function postCommand($serial, $command) {
        return $this->postApi('gatewayApi/command/commandkey/' . $serial . '/' . $command);
    }
}