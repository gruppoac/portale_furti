<?php

namespace AppBundle\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class ApiDmsManager
{
    protected $apiKey;
    protected $host;
    protected $client;
    protected $path = '/v1/dmsisa/';
    protected $lifetime = 3600;

    public function __construct($host, $path, $apiKey) {
        $this->apiKey = $apiKey;
        $this->host = $host;
        $this->path = $path;
        
        $this->client = new Client(
                array('base_uri' => $this->host,
                    'headers' => array(
                        'X-API-KEY' => $this->apiKey
                    )
                )
        );
    }
    
    public function getApi($uri, $querystring = array() ) {
        try {
            $response = $this->client->get($this->path.$uri, array(
                'query' => $querystring
            ));
            return json_decode( (string)$response->getBody(), true);
        }
        catch (ClientException $exception) {
            return $exception->getCode();
        }
    }

    public function getCarStatus($plate, $params = array()) {
        return $this->getApi('/carStatus/' . $plate, $params);
    }

    public function getCarsByVoucher($voucher, $params = array()) {
        return $this->getApi('/carsByVoucher/' . $voucher, $params);
    }

    public function getCarByPlate($plate, $params = array()) {
        return $this->getApi('/carByPlate/' . $plate, $params);
    }

    public function getDeviceStatus($serial, $params = array()) {
        return $this->getApi('/deviceStatus/' . $serial, $params);
    }

    public function getDeviceBySerial($serial, $params = array()) {
        return $this->getApi('/deviceBySerial/' . $serial, $params);
    }

    public function getDeviceById($id, $params = array()) {
        return $this->getApi('/deviceById/' . $id, $params);
    }
}