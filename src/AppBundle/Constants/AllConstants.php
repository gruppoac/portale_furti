<?php
namespace AppBundle\Constants;

class AllConstants {
    const CONFIGURATION_COMMAND = 'GPR-Dwl:ON';
    const IDZBFILEDOWNLOAD = 69;
    const FILEPATHONZBOX = "0:/ConfUpdate.txt";
    const PROD_CONFIGURATION_COMMAND = "GPRT.RLTR=;60";
    const TEST_COMMAND = "ZB-Test:;3";
    const IDBLACKBOXTYPE = 4;
    const RADIUS = 300;
    const INSTALL_AZIENDA_ID = 50;
    const UNINSTALL_AZIENDA_ID = 1;
    const COMMESSA_CODE = "285.01";
    const GENR_PDTS = "GENR.PDTS=;0";
    const GENR_PDTS1 = "GENR.PDTS=;1";
    const ZB_CALB = "ZB-Calb:OFF";
    const GENR_TTPD = "GENR.TTPD=;360";
    const GENR_TTPD_0 = "GENR.TTPD=;0";
    const GENR_TTPO = "GENR.TTPO=;28800";
    const GENR_TTFD = "GENR.TTFD=;29400";
    const GENR_CFGT = "GENR.CFGT=;1";
    const GENR_CFGT_0 = "GENR.CFGT=;0";
    const ZB_REST_IMM = "ZB-Rest:IMM";
    const GPRT_RLTR_60 = "GPRT.RLTR=;60";
    const ZB_INFO = "ZB-Info:";
    const TRK_EME = "TRK-EME:";
    const TRK_EME_OFF = "TRK-EME:OFF";
}