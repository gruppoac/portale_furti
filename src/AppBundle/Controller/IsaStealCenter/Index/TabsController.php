<?php

namespace AppBundle\Controller\IsaStealCenter\Index;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\IsaSteals;
use AppBundle\Helper\Date;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/stealCenter/list/tabs")
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class TabsController extends AbstractController
{
    
    /**
     * @todo contatori
     * @Route("", name="isa_steal_center_list_tabs")
     */
    public function tabAction(Request $request) {
        $todo = 0;
        $working = 0;
        $pending = 0;
        $closed = 0;
         
        return $this->render('stealCenter/index/tabs.html.twig', array(
            'tabActive' => $request->query->get('tabActive'),
            'todo' => $todo,
            'pending' => $pending,
            'working' => $working,
            'closed' => $closed,
        ));        
    }
}