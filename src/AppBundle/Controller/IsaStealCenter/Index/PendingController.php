<?php

namespace AppBundle\Controller\IsaStealCenter\Index;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\IsaSteals;
use AppBundle\Helper\Date;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/stealCenter/pending")
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class PendingController extends AbstractController {

    /**
     * Pagina
     * @Route("", name="isa_steal_center_list_pending")
     */
    public function indexAction() {
        return $this->render('stealCenter/index/pending.html.twig', array());
    }

    /**
     * Dati della tabella in formato json (usato da dataTables)
     * @Route("/steal.json", name="isa_steal_center_list_pending_json")
     * @Security("has_role('ROLE_ISASTEALCENTER')")
     */
    public function tableJsonAction(Request $request) {
        $status = $this->getStealsDocManager()->getRepository("AppBundle:IsaStealsStatus")->findOneByIsaStealStatusId(3);

        $total = $this->getRecordsTotal($request, $status);
        $filtered = $this->getRecordsFiltered($request, $status);
        $steals = $this->getRecords($request, $status);

        $json = array(
            "recordsTotal" => $total,
            "recordsFiltered" => $filtered,
            'data' => $this->getJsonRow( $steals )
        );

        return new JsonResponse($json);
    }


}
