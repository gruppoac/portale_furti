<?php

namespace AppBundle\Controller\IsaStealCenter\Index;


use AppBundle\Entity\IsaSteals;
use AppBundle\Helper\Date;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Helper\LabelStatus;
use AppBundle\Constants\AllConstants;

abstract class AbstractController extends Controller
{
    /**
     * @return ApiGatewayManager
     */
    protected function getGatewayApi() {
        return $this->get('api_gateway');
    }

    /**
     * 
     * @param IsaSteals $items
     * @return array
     */
    protected function getJsonRow($items) {
        $isStealAdmin = $this->hasStealAdminRole();
        $data = array();
        foreach($items as $item) {
            $url = $item->getIsaStealStatus()->getIsaStealStatusId() == 2 ?
                    $this->generateUrl('isastealcenter_handled', array('stealId' => $item->getIsaStealId()) ) :
                    $this->generateUrl('isastealcenter_handle', array('stealId' => $item->getIsaStealId()) );
            $etrackingStatus = $this->getEmergencyTrackingInfo($item);
            $data[] = array(
                'url'                           => $url,
                'trackingClass'                 => $this->stealTrackingClass($etrackingStatus) ,
                'isaStealType'                  => $item->getIsaStealType()->getIsaStealTypeDescription(),
                'isaStealCarPlate'              => $item->getIsaStealCarPlate(),
                'isaStealCarModel'              => $item->getIsaStealCarModel(),
                'isaStealDeviceSn'              => $item->getIsaStealDeviceSn(),
                'isaStealHolderTel'             => $item->getIsaStealHolderTel(),
                'isaStealCarInsuranceNumber'    => $item->getIsaStealCarInsuranceNumber(),
                'isaStealHolderName'            => $item->getIsaStealHolderName(),
                'isaStealDate'                  => Date::gmtDataTime( $item->getIsaStealDate() ),
                'isaStealUser'                  => $item->getIsaStealUser() ?: '---' ,
                'isaStealStatus'                => $item->getIsaStealStatus()->getIsaStealStatusDescription(),
                'isaStealTracking'              => LabelStatus::isaStealLabelTracking($etrackingStatus),
                'accessible'                    => ($item->getIsaStealStatus()->getIsaStealStatusId() != 1 || $this->getUser()->getUsername() ==  $item->getIsaStealUser() || $isStealAdmin) ? 1 : 0,
            );
        }
        
        return $data;
    }

    protected function getRecordsTotal(Request $request, $status = array() ) {
        $qb = $this->getQb($request, $status);
        return $qb->select('COUNT(s) AS tot')->getQuery()->getSingleScalarResult();                
    }
    
    protected function getRecordsFiltered(Request $request, $status = array() ) {
        $qb = $this->getQb($request, $status);
        $this->getQbCriteria($request, $qb);        
        return $qb->select('COUNT(s) AS tot')->getQuery()->getSingleScalarResult();                
    }
    
    protected function getRecords(Request $request, $status = array() ) {
        $qb = $this->getQb($request, $status);
        $this->getQbCriteria($request, $qb);        
        $this->getQbPaginate($request, $qb);
        $results = $qb->getQuery()->getResult();
        return $results;
        
    }
    
    /**
     * 
     * @param Request $request
     * @param string $status
     * @return QueryBuilder
     */
    protected function getQb(Request $request, $status = array() ) {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository("AppBundle:IsaSteals")->createQueryBuilder("s");
        //->groupBy('d.deviceId') ;
                
        if($status ) {
            $qb->andWhere("s.isaStealStatus IN(:status)")->setParameter('status', $status );
        }
        
        return $qb;

    }

    protected function getQbCriteria(Request $request, QueryBuilder $qb) {
        $search = null;
        $search_data = $request->query->get('search');

        if ($search_data) {
            $search = ($search_data['value']) ? $search_data['value'] . '%' : null;
        }

        if ($search_data && $search) {
            $qb->andWhere("s.isaStealCarPlate LIKE :plate")->setParameter(":plate", $search);
        }
    }
    
    protected function getQbPaginate(Request $request, QueryBuilder $qb) {
        $length = ($request->query->get('length')) ? intval($request->query->get('length')) : 10;
        $start = ($request->query->get('start')) ? intval($request->query->get('start')) : 0;

        if ($length != -1) {
            $qb->setFirstResult($start);
            $qb->setMaxResults($length);
        }
        
        $order = $request->query->get('order');
        if ($order) {
            $column_id = ($order[0]['column'] != 0) ? $order[0]['column'] : 0;
            $columns = $request->query->get('columns');
            //if($columns[$column_id]['data'] == "form") {
            //    $columns[$column_id]['data'] = "carBookingDate";
            //}
            $column = 's.' . $columns[$column_id]['data'];
            $dir = ($order[0]['dir']) ? $order[0]['dir'] : 'asc';
        }        
        if ($order) {
            $qb->addOrderBy($column, $dir);
        }
    }

    protected function stealTrackingClass($status) {
        switch ($status) {
            case 0:
                return "red-bg";
            case 1:
                return "green-bg";
            case 2:
                return "yellow-bg";
            case 3:
                return "yellow-bg";
            case 4:
                return "red-bg";
            default:
                return "red-bg";
        }
    }

    protected function getStealsDocManager() {
        return $this->getDoctrine()->getManager();
    }

    protected function getEmergencyTrackingInfo(IsaSteals $steal) {
        $type = 'OFF';
        $time = null;
        $commands = $this->getGatewayApi()->getLastCommands($steal->getIsaStealDeviceSn(), AllConstants::TRK_EME, 10);
        $current = $this->getGatewayApi()->getLastCommands($steal->getIsaStealDeviceSn(), AllConstants::TRK_EME, 1);

        if (is_numeric($commands) || is_numeric($current) || is_null($commands) || is_null($current)) {
            return ($commands == 404 || $current == 404) ? 0 : 4;
        }

        if (isset($current[0]['command']) && $current[0]['command'] != AllConstants::TRK_EME_OFF) {
            $split = explode(':', $current[0]['command']);
            $time = isset($split[1]) ? $split[1] : null;
            $type = 'ON';
        }

        if ($type == 'OFF') {
            if ($current[0]['stato'] == 1) {
                $this->updateTracking($steal, 0);
                return 0;
            }

            foreach ($commands as $command) {
                if ($command['command'] != AllConstants::TRK_EME_OFF) {
                    break;
                }
                if ($command['stato'] == 1) {
                    $this->updateTracking($steal, 0);
                    return 0;
                }
            }
            $this->updateTracking($steal, 2);
            return 2;
        }

        if ($type == 'ON') {
            if ($current[0]['stato'] == 1) {
                $this->updateTracking($steal, 1);
                return 1;
            }

            foreach ($commands as $command) {
                if ($command['command'] == AllConstants::TRK_EME_OFF || $command['command'] != AllConstants::TRK_EME . $time) {
                    break;
                }
                if ($command['stato'] == 1) {
                    $this->updateTracking($steal, 1);
                    return 1;
                }
            }
            $this->updateTracking($steal, 3);
            return 3;
        }
    }

    protected function updateTracking(IsaSteals $steal, $status) {
        $em = $this->getStealsDocManager();
        $steal->setIsaStealTracking($status);
        $em->persist($steal);
        $em->flush();
    }

    protected function hasStealAdminRole() {
        $isStealAdmin = false;
        $currentUser = $this->getUser();
        $currentUserRoles = $currentUser->getRoles();
        foreach ($currentUserRoles as $currentUserRole) {
            if ($currentUserRole == 'ROLE_ADMIN_ISASTEALCENTER') {
                $isStealAdmin = true;
                continue;
            }
        }
        return $isStealAdmin;
    }

}