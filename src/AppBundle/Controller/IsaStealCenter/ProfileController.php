<?php

namespace AppBundle\Controller\IsaStealCenter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;


/**
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 * @Route("/stealCenter/profile")
 */
class ProfileController extends AbstractStealController {

    /**
     * @Route("", name="steal_profile")
     */
    public function indexAction(Request $request) {
        return $this->render('stealCenter/profile/index.html.twig', array());
    }

}
