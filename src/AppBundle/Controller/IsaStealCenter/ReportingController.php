<?php

namespace AppBundle\Controller\IsaStealCenter;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\IsaStealsTypes;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Helper\LabelStatus;
use AppBundle\Entity\IsaSteals;
use AppBundle\Entity\Cars;
use AppBundle\Entity\Devices;
use AppBundle\Form\IsaSteal\CarReportingType;
use AppBundle\Model\IsaSteal\CarReportingModel;
use AppBundle\Form\IsaSteal\HolderReportingType;
use AppBundle\Model\IsaSteal\HolderReportingModel;

/**
 * @Route("/stealCenter/reporting")
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class ReportingController extends AbstractStealController
{
    /**
     * @Route("", name="isastealcenter_search")
     */
    public function blockAction(Request $request)
    {
        $model = new CarReportingModel;
        $form = $this->createForm(CarReportingType::class, $model, array('action' => $this->generateUrl('isastealcenter_search')));
        $form->handleRequest($request);
        if ($form->isValid()) {
            try {
                $steal = $this->reportingByCar($model);
                $this->setStealLog($steal, 1);
                return $this->redirectToRoute('isastealcenter_handle', array('stealId' => $steal->getIsaStealId() ) );
            }
            catch(\Exception $e) {
                $this->setExceptionMessage('error', $e->getMessage() );
            }
        }

        $modelHolder = new HolderReportingModel();
        $formHolder = $this->createForm(HolderReportingType::class, $modelHolder, array('action' => $this->generateUrl('isastealcenter_search')));
        $dataHolder = $request->request->get('holder_reporting');
        ($dataHolder['plate']) ? $modelHolder->setPlate($dataHolder['plate']) : null;
        $formHolder->handleRequest($request);

        if ($formHolder->isValid()) {
            try {
                $steal = $this->reportingByHolder($modelHolder);
                $this->setStealLog($steal, 1);
                return $this->redirectToRoute('isastealcenter_handle', array('stealId' => $steal->getIsaStealId() ) );
            }
            catch(\Exception $e) {
                $this->setExceptionMessage('error', $e->getMessage() );
            }
        }

        return $this->render('stealCenter/reporting/index.html.twig', array(
            'form'          => $form->createView(),
            'formHolder'    => $formHolder->createView()
        ));
    }

    /**
     * @param CarReportingModel $model
     * @return IsaSteals
     * @throws \Exception
     */
    protected function reportingByCar(CarReportingModel $model) {
        $plate = trim($model->getPlate());
        $car = $this->validationCarResponse($this->getDmsApi()->getCarByPlate($plate), $model, 13);

        if ( in_array(intval($car['car']['car_status']), array(0,1,6)) ) {
            $labelStatus = LabelStatus::getCarStatus(intval($car['car']['car_status']));
            $note = array(
                "message"   => 'La targa ' . $plate . ' risulta "' . $labelStatus . '"',
                "plate"     => ($plate) ? $plate : null,
                "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
            );
            $this->setStealLog(null, 13, $note);
            throw new \Exception($note['message']);
        }

        $this->hasSteal($model);
        $apiCall = $this->getGatewayApi()->getVoucher(trim($car['car']['car_voucher_number']));
        $voucher = (is_string($apiCall) || is_numeric($apiCall)) ? null : $apiCall;
        $device = $this->validationDeviceResponse($this->getDmsApi()->getDeviceById(intval($car['car']['car_device_fk'])), $model, 13);
        $coordinates = $this->getLocalizationInfo($device['device']['device_sn']);
        $date = ($model->getDate()) ? $model->getDate() : null;
        $steal = $this->createSteal($car['car'], $device['device'], $voucher, $date, $coordinates, $model->getTipo()->getIsaStealTypeId(), $model->getComplaint());
        return $steal;
    }

    /**
     * @Route("/car_json", name="isastealcenter_car_json")
     */
    public function getCarData(Request $request) {
        $plate = trim($request->query->get('plate'));
        if ($plate == null) {
            $response['empty'] = true;
            $response['valid'] = false;
            $response['car'] = '<div class="row"><div class="col-lg-12"><p class="plate-status" style="color: red">La targa <span class="plate"><b>ND</b> risulta "</span><span class="status"><b>ND</b></span>"<span class="note"> Torna allo step precedente e verifica la targa!</span></p></div></div><div class="row" id="rc-confirm-plate"><div class="row"><div class="col-lg-9 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="holder"><label>Intestatario</label><p>ND</p></div><div class="model"><label>Modello</label><p>ND</p></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="color"><label>Colore</label><p>ND</p></div><div class="contract"><label>Contratto</label><p>ND</p></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="tel"><label>Tel primario</label><p>ND</p></div><div class="aggiuntivi"><label>Tel aggiuntivi</label><p>ND</p></div></div></div></div></div>';
            return new JsonResponse($response);
        }
        $apiCall = $this->getDmsApi()->getCarByPlate($plate);
        $car = (is_numeric($apiCall)) ? null : $apiCall;
        if (!$car || ($car && (!isset($car['car']) || empty($car['car'])))) {
            $response['empty'] = true;
            $response['valid'] = false;
            $response['car'] = '<div class="row"><div class="col-lg-12"><p class="plate-status" style="color: red">La targa <span class="plate"><b>' .  $plate . '</b> risulta "</span><span class="status"><b>ND</b></span>"<span class="note"> Torna allo step precedente e verifica la targa!</span></p></div></div><div class="row" id="rc-confirm-plate"><div class="row"><div class="col-lg-9 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="holder"><label>Intestatario</label><p>ND</p></div><div class="model"><label>Modello</label><p>ND</p></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="color"><label>Colore</label><p>ND</p></div><div class="contract"><label>Contratto</label><p>ND</p></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="tel"><label>Tel primario</label><p>ND</p></div><div class="aggiuntivi"><label>Tel aggiuntivi</label><p>ND</p></div></div></div></div></div>';
            return new JsonResponse($response);
        }
        $apiCall = $this->getGatewayApi()->getVoucherByPlate($plate);
        $voucher = (is_numeric($apiCall)) ? null : $apiCall;
        if (!$voucher || empty($voucher)) {
            $response['empty'] = true;
            $response['valid'] = false;
            $response['car'] = '<div class="row"><div class="col-lg-12"><p class="plate-status" style="color: red">La targa <span class="plate"><b>' .  $plate . '</b> risulta "</span><span class="status"><b>ND</b></span>"<span class="note"> Torna allo step precedente e verifica la targa!</span></p></div></div><div class="row" id="rc-confirm-plate"><div class="row"><div class="col-lg-9 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="holder"><label>Intestatario</label><p>ND</p></div><div class="model"><label>Modello</label><p>ND</p></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="color"><label>Colore</label><p>ND</p></div><div class="contract"><label>Contratto</label><p>ND</p></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="tel"><label>Tel primario</label><p>ND</p></div><div class="aggiuntivi"><label>Tel aggiuntivi</label><p>ND</p></div></div></div></div></div>';
            return new JsonResponse($response);
        }
        /*$carInfo = array(
            'car_plate'         => $plate,
            'car_model'         => ($car && isset($car['car']) && !empty($car['car']) ) ? $car['car']['car_model'] : null,
            'car_status'        => ($car && isset($car['car']) && !empty($car['car']) ) ? $car['car']['car_status'] : null,
            'car_label_status'  => ($car && isset($car['car']) && !empty($car['car']) ) ? LabelStatus::getCarStatus(intval($car['car']['car_status'])) : null,
            'car_status_color'  => ($car && isset($car['car']) && !empty($car['car'] && in_array($car['car']['car_status'], array(2,3,4,5))) ) ? "black" : "red",
            'car_contract'      => $voucher['contratto'],
            'car_color'         => $voucher['colore'],
            'holder_tel'        => $voucher['tel'],
            'holder_aggiuntivi' => $voucher['aggiuntivi'],
            'holder'            => $voucher['nome'] . ' ' . $voucher['cognome']
        );
        $response['note'] = array();
        $response['car'] = $carInfo;*/

        $valid = (isset($car["car"]["car_status"]) && in_array($car['car']['car_status'], array(2,3,4,5))) ? true : false;
        $carLabelStatus = (isset($car['car']['car_status'])) ? LabelStatus::getCarStatus(intval($car['car']['car_status'])) : "ND";
        $carLabelColor = (isset($car['car']['car_status']) && in_array($car['car']['car_status'], array(2,3,4,5))) ? "inherit" : "red";
        $holderCar = ($voucher["nome"] && $voucher["cognome"]) ? $voucher["nome"] . ' ' . $voucher["cognome"] : "ND";
        $carModel = (isset($car["car"]["car_model"])) ? $car["car"]["car_model"] : "ND";
        $carColor = ($voucher["colore"]) ? $voucher["colore"] : "ND";
        $contract = ($voucher["contratto"]) ? $voucher["contratto"] : "ND";
        $holderPhone = ($voucher["tel"]) ? $voucher["tel"] : "ND";
        $holderPhones = ($voucher["aggiuntivi"]) ? $voucher["aggiuntivi"] : "ND";
        $note = ($valid) ? "Verifica i dati prima di proseguire" : "Torna allo step precedente e verifica la targa!";

        $response['empty'] = false;
        $response['valid'] = $valid;
        $response['car'] = '<div class="row"><div class="col-lg-12"><p class="plate-status" style="color:' . $carLabelColor . '">La targa <span class="plate"><b>' . $plate . '</b> risulta "</span><span class="status"><b>' . $carLabelStatus . '</b></span>"<span class="note"> ' . $note . '</span></p></div></div><div class="row" id="rc-confirm-plate"><div class="row"><div class="col-lg-9 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="holder"><label>Intestatario</label><p>' . $holderCar . '</p></div><div class="model"><label>Modello</label><p>' . $carModel . '</p></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="color"><label>Colore</label><p>' . $carColor . '</p></div><div class="contract"><label>Contratto</label><p>' . $contract . '</p></div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><div class="tel"><label>Tel primario</label><p>' . $holderPhone . '</p></div><div class="aggiuntivi"><label>Tel aggiuntivi</label><p>' . $holderPhones . '</p></div></div></div></div></div>';

        return new JsonResponse($response);
    }

    /**
     * @param HolderReportingModel $model
     * @return IsaSteals
     * @throws \Exception
     */
    protected function reportingByHolder(HolderReportingModel $model) {
        $car = $this->validationCarResponse($this->getDmsApi()->getCarByPlate(trim($model->getPlate())), $model, 14);
        $device = $this->validationDeviceResponse($this->getDmsApi()->getDeviceById($car['car']['car_device_fk']), $model, 13);
        $apiCall = $this->getGatewayApi()->getVoucherByPlate(trim($car['car']['car_plate']));
        $voucher = (is_string($apiCall) || is_numeric($apiCall)) ? null : $apiCall;
        $coordinates = $this->getLocalizationInfo($device['device']['device_sn']);
        $steal = $this->createSteal($car['car'], $device['device'], $voucher, $model->getDate(), $coordinates, $model->getTipo()->getIsaStealTypeId(), $model->getComplaint());
        return $steal;
    }

    /**
     * @Route("/voucher_cars_json", name="isastealcenter_voucher_json")
     */
    public function getVoucherData(Request $request) {
        $installedCars = array();
        $otherCars = array();
        $apiCall = $this->getGatewayApi()->getVoucherByHolder($request->get('name'), $request->get('surname'));
        $vouchers = (is_string($apiCall) || is_numeric($apiCall)) ? null : $apiCall;
        if (!$vouchers) {
            $note = "Contratto non presente per il cliente " . $request->get('name') . " " . $request->get('surname');
            $this->setStealLog(null, 14, $note);
            $response['note'] = array($note);
            $response['cars'] = array();
            return new JsonResponse($response);
        }

        foreach ($vouchers as $voucher) {
            $apiDmsCall = $this->getDmsApi()->getCarsByVoucher($voucher['voucher']);
            $data = (is_string($apiDmsCall) || is_numeric($apiDmsCall)) ? null : $apiDmsCall;
            $cars = $data['cars'];
        }

        if (empty($cars)) {
            $note = "Nessuna vettura trovata per il cliente " . $request->get('name') . " " . $request->get('surname');
            $this->setStealLog(null, 14, $note);
            $response['note'] = array($note);
            $response['cars'] = array();
            return new JsonResponse($response);
        }

        foreach ($cars as $car) {
            if (!in_array( intval($car['car_status']), array(0,1,6) )) {
                $installedCars[] = array(
                    'plate'     => $car['car_plate'],
                    'model'     => $car['car_model'],
                    'status'    => LabelStatus::getCarStatus($car['car_status']),
                    'hasSteal'  => ($this->getStealByPlate($car['car_plate'])) ? true : false
                );
            } else {
                $otherCars[] = array(
                    'plate'     => $car['car_plate'],
                    'model'     => $car['car_model'],
                    'status'    => LabelStatus::getCarStatus($car['car_status']),
                    'hasSteal'  => ($this->getStealByPlate($car['car_plate'])) ? true : false
                );
            }
        }

        if (empty($installedCars)) {
            $note = "Nessuna targa associata al cliente " . $request->get('name') . " " . $request->get('surname') . " risulta installata!";
            $this->setStealLog(null, 14, $note);
            $response['note'] = array($note);
            $response['cars'] = array();
            return new JsonResponse($response);
        }

        $response['note'] = array();
        $response['cars'] = $installedCars;
        return new JsonResponse($response);
    }

    protected function createSteal($car, $device, $voucher = null, $date = null, $coordinates = null, $type, $complaint = null) {
        $user = $this->getUser();
        $em = $this->getStealsDocManager();
        $status = $em->getRepository('AppBundle:IsaStealsStatus')->findOneByIsaStealStatusId(1);
        $type = $em->getRepository('AppBundle:IsaStealsTypes')->findOneByIsaStealTypeId($type);
        $steal = new IsaSteals();

        $steal->setIsaStealCarPlate($car['car_plate']);
        $steal->setIsaStealCarModel($car['car_model']);
        $steal->setIsaStealCarColor(($voucher) ? $voucher['colore'] : null);
        $steal->setIsaStealDeviceSn($device['device_sn']);
        $steal->setIsaStealStatus($status);
        $steal->setIsaStealType($type);
        $steal->setIsaStealInsertDate(new \DateTime());
        $steal->setIsaStealDate(($date) ? $date : NULL);
        $steal->setIsaStealHolderTel(($voucher) ? $voucher['tel'] : null);
        $steal->setIsaStealHolderName(($voucher) ? $voucher['nome'] . ' ' . $voucher['cognome'] : null);
        $steal->setIsaStealCarInsuranceNumber(($voucher) ? $voucher['contratto'] : null);
        $steal->setIsaStealLat(($coordinates) ? $coordinates[0]['BLat'] : '0');
        $steal->setIsaStealLong(($coordinates) ? $coordinates[0]['BLong'] : '0');
        $steal->setIsaStealComplaint(($complaint) ? $complaint  : null);
        $steal->setIsaStealUser($user->getUsername());

        $em->persist($steal);
        $em->flush();

        return $steal;
    }

}