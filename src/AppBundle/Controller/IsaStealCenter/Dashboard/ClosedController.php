<?php

namespace AppBundle\Controller\IsaStealCenter\Dashboard;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\Filter\FilterType;
use AppBundle\Model\Filter\FilterModel;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;


/**
 * @Route("/stealCenter/dashboard")
 * @Security("has_role('ROLE_ADMIN_ISASTEALCENTER')")
 */
class ClosedController extends AbstractStealController
{
    /**
     * @Route("/closed", name="isastealcenter_dashboard_closed_table")
     */
    public function closedAction(Request $request) {

        $filter = new FilterModel;
        $form = $this->createForm(FilterType::class, $filter);
        $form->handleRequest($request);

        $repo = $this->getDoctrine()->getManager()->getRepository("AppBundle:IsaSteals");
        $results = $repo->getRecordsByFilter($filter, "2");
        $tot = $repo->getCountByFilter($filter,"2");

        $html = $this->renderView('stealCenter/dashboard/closedTable.html.twig', array(
            'results'       => $results,
            'form'          => $form->createView(),
            'filter'        => $filter,
            'tot'           => $tot
        ));

        $data = array(
            'html' => $html
        );
        return new JsonResponse($data);

    }

    /**
     * @Route("/closed/report", name="isastealcenter_dashboard_closed_report")
     */
    public function pendingReportAction(Request $request) {
        $documentRoot = $request->server->get('DOCUMENT_ROOT');
        $workingDirectory = str_replace("/web","",$documentRoot,$i);
        $commandline = "bin/console steals:dashboard-report --status=2";

        $process = new Process($commandline);
        $process->setWorkingDirectory($workingDirectory);
        $process->start();

        $process->wait(function ($type, $buffer) {
            if (Process::ERR === $type) {
                throw new \Exception("Errore nella generazione del report");
            }
        });
        return $this->getReport("closed");
    }



}