<?php

namespace AppBundle\Controller\IsaStealCenter\Dashboard;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\Filter\FilterType;
use AppBundle\Model\Filter\FilterModel;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * @Route("/stealCenter/dashboard")
 * @Security("has_role('ROLE_ADMIN_ISASTEALCENTER')")
 */
class PendingController extends AbstractStealController
{
    /**
     * @Route("/pending", name="isastealcenter_dashboard_pending_table")
     */
    public function pendingAction(Request $request) {

        $filter = new FilterModel;
        $form = $this->createForm(FilterType::class, $filter);
        $form->handleRequest($request);

        $repo = $this->getDoctrine()->getManager()->getRepository("AppBundle:IsaSteals");
        $results = $repo->getRecordsByFilter($filter,"3");
        $tot = $repo->getCountByFilter($filter,"3");

        $html = $this->renderView('stealCenter/dashboard/pendingTable.html.twig', array(
            'results'       => $results,
            'form'          => $form->createView(),
            'filter'        => $filter,
            'tot'           => $tot
        ));

        $data = array(
            'html' => $html
        );
        return new JsonResponse($data);

    }

    /**
     * @Route("/pending/report", name="isastealcenter_dashboard_pending_report")
     */
    public function pendingReportAction(Request $request) {
        $documentRoot = $request->server->get('DOCUMENT_ROOT');
        $workingDirectory = str_replace("/web","",$documentRoot,$i);
        $commandline = "bin/console steals:dashboard-report --status=3";
        $process = new Process($commandline);
        $process->setWorkingDirectory($workingDirectory);
        $process->start();

        // ... fare altre cose

        $process->wait(function ($type, $buffer) {
            if (Process::ERR === $type) {
                throw new \Exception("Errore nella generazione del report");
            }
        });
        return $this->getReport("pending");
    }

}