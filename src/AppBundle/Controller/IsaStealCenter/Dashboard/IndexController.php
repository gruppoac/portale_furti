<?php

namespace AppBundle\Controller\IsaStealCenter\Dashboard;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\Filter\FilterType;
use AppBundle\Model\Filter\FilterModel;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;


/**
 * @Route("/stealCenter/dashboard")
 * @Security("has_role('ROLE_ADMIN_ISASTEALCENTER')")
 */
class IndexController extends AbstractStealController
{
    /**
     * @Route("", name="isastealcenter_dashboard")
     */
   public function indexController() {
       return $this->render('stealCenter/dashboard/index.html.twig');
   }

    /**
     * @Route("/table", name="isastealcenter_dashboard_audit_table")
     */
    public function tableAction(Request $request) {
        $audit = $this->auditTable($request);
        $html = $this->renderView('stealCenter/dashboard/table.html.twig', array(
            'audit'     => $audit
        ));

        $data = array(
            'html' => $html
        );

        return new JsonResponse($data);

    }

    protected function auditTable(Request $request) {
        $filter = new FilterModel;
        $form = $this->createForm(FilterType::class, $filter);
        $form->handleRequest($request);
        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:IsaStealsLog');
        $results = $repo->getRecordsByFilter($filter);
        $tot = $repo->getCountByFilter($filter);

        $data = array(
            'results' => $results,
            'form'    => $form->createView(),
            'filter'  => $filter,
            'tot'     => $tot
        );
        return $data;
    }


}