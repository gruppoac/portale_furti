<?php

namespace AppBundle\Controller\IsaStealCenter\Handle;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Constants\AllConstants;
use AppBundle\Entity\IsaSteals;
use AppBundle\Helper\Date;
use \DateTime;



/**
 * @Route("/stealCenter/handle/{isaStealId}")
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class WaitController extends AbstractStealController
{
    /**
     * @Route("/awaiting", name="isastealcenter_awaiting")
     * @Method("POST")
     */
    public function awaitingAction(IsaSteals $steal) {
        $em = $this->getStealsDocManager();
        $user = $this->getUser();

        $steal->setIsaStealStatus($em->getRepository('AppBundle:IsaStealsStatus')->findOneByIsaStealStatusId(3));
        $steal->setIsaStealUser($user->getUsername());
        $em->persist($steal);
        $em->flush();

        $this->setStealLog($steal, 4);

        $response = array();
        $response['status'] = "event-closed";
        $response['message'] = "Evento chiuso con successo";
        $response['error'] = 0;

        return new JsonResponse($response);
    }

}