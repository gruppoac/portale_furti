<?php

namespace AppBundle\Controller\IsaStealCenter\Handle;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Constants\AllConstants;
use AppBundle\Entity\IsaSteals;
use AppBundle\Helper\Date;
use \DateTime;

/**
 * 
 * @Route("/stealCenter/handle/{isaStealId}")
 * @ParamConverter("steal", class="AppBundle:IsaSteals", options={"id" = "isaStealId"})
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class XhrController extends AbstractStealController
{
    
    /**
     * @Route("/xhr_maps", name="isastealcenter_xhr_maps")
     */
    public function xhrMapsAction(Request $request, IsaSteals $steal)
    {

        $device = $this->getDeviceBySn($steal->getIsaStealDeviceSn());
        if(!$device) {
            throw $this->createNotFoundException("Device non trovato");
        }

        $event_info = $this->getSerializer()->toArray( $steal );
        $localization_info = $this->getLocalizationInfo($device->getDeviceSn());
        $event_info["isaEventDate"] = Date::gmtData( $event_info["isa_steal_date"] );

        $response = array();
        $response['status'] = "event-info-retrieved";
        $response['message'] = "Dati evento e ultima localizzazione ottenuti";
        $response['error'] = 0;
        $response['event_info'] = $event_info;
        $response['localization_info'] = $localization_info;

        return new JsonResponse($response);
    }

   
}