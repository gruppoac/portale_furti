<?php

namespace AppBundle\Controller\IsaStealCenter\Handle;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Constants\AllConstants;
use AppBundle\Entity\IsaSteals;
use AppBundle\Helper\Date;
use \DateTime;


/**
 * @Route("/stealCenter/handle/{isaStealId}")
 * @ParamConverter("steal", class="AppBundle:IsaSteals", options={"id" = "isaStealId"})
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class CloseController extends AbstractStealController
{    
    /**
     * @Route("/close", name="isastealcenter_close")
     * @Method("POST")
     */
    public function closeEventAction(Request $request, IsaSteals $steal)
    {
        $em = $this->getStealsDocManager();
        $latitude = $request->request->get('BLat', null);
        $longitude = $request->request->get('BLong', null);
        $inputValue = $request->request->get('inputValue', null);

        if (null == $latitude && null == $longitude && null != $inputValue) {
            $response = array();
            $response['status'] = "event-not-closed";
            $response['message'] = "Errore nella chiusura dell'evento. Aggiornare e riprovare o contattare l'assistenza";
            $response['error'] = 1;
            $response['error_code'] = "CE001ER";
            return new JsonResponse($response);
        }

        $etracking = $this->getEmergencyTrackingInfo($steal);
        if ($etracking && $etracking['type'] === 'ON') {
            $response = $this->trackingDisable($steal->getIsaStealDeviceSn());
            if (!$response) {
                $response = array();
                $response['status'] = "event-not-closed";
                $response['message'] = "Errore nella chiusura dell'evento. Aggiornare e riprovare o contattare l'assistenza";
                $response['error'] = 1;
                $response['error_code'] = "CE002ER";
                return new JsonResponse($response);
            }
            $this->updateTracking($steal, 2);
        }

        $steal->setIsaStealStatus($em->getRepository('AppBundle:IsaStealsStatus')->findOneByIsaStealStatusId(2));
        $steal->setIsaStealHandledLat( $latitude );
        $steal->setIsaStealHandledLong( $longitude );
        $steal->setIsaStealUpdateDate(new \DateTime() );
        $steal->setIsaStealNote( $inputValue );
        $em->persist($steal);
        $em->flush();

        $this->setStealLog($steal, 3);

        $response = array();
        $response['status'] = "event-closed";
        $response['message'] = "Evento chiuso con successo";
        $response['error'] = 0;

        return new JsonResponse($response);
    }
    
}