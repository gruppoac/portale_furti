<?php

namespace AppBundle\Controller\IsaStealCenter\Handle;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Constants\AllConstants;
use AppBundle\Entity\IsaSteals;


/**
 * @Route("/stealCenter/handle/{isaStealId}")
 * @ParamConverter("steal", class="AppBundle:IsaSteals", options={"id" = "isaStealId"})
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class TrackingController extends AbstractStealController
{

    /**
     * @Route("/tracking/set_command/{time}/{status}", name="isastealcenter_tracking_set_command")
     * @Method("POST")
     */
    public function trackingSetCommandAction(IsaSteals $steal, $time, $status) {
        $response = array();
        if ($status == 'OFF') {
            $commandId = $this->trackingEnable($steal->getIsaStealDeviceSn(), $time);
            ($time == 30) ? $this->setStealLog($steal, 5) : $this->setStealLog($steal, 6);
        } else if ($status == 'ON') {
            $commandId = $this->trackingDisable($steal->getIsaStealDeviceSn());
            $this->setStealLog($steal, 7);
        } else {
            $response = array();
            $response['message'] = "Comando non inviato..riprovare o contattare l'assistenza";
            $response['error'] = 1;
            return new JsonResponse($response);
        }

        ($status == 'OFF') ? $this->updateTracking($steal, 3) : $this->updateTracking($steal, 2);
        $response['message'] = "Comando inserito ma in attesa di esecuzione...";
        $response['error'] = 0;
        $response['commandId'] = $commandId;

        return new JsonResponse($response);
    }        
    
    /**
     * @Route("/tracking/status", name="isastealcenter_tracking_status")
     */
    public function trackingStatusAction(IsaSteals $steal, Request $request) {
        $response = array();
        $type = 'OFF';
        $time = null;
        $commandId = $request->query->get('commandId', null);
        $commands = $this->getGatewayApi()->getLastCommands($steal->getIsaStealDeviceSn(), AllConstants::TRK_EME, 10);
        $current = (!$commandId) ? $this->getGatewayApi()->getLastCommands($steal->getIsaStealDeviceSn(), AllConstants::TRK_EME, 1) : $this->getGatewayApi()->getCommandById($commandId);

        if (is_numeric($commands) || is_numeric($current) || is_null($commands) || is_null($current)) {
            if ($commands == 404 || $current == 404 || is_null($commands) || is_null($current)) {
                $response['message'] = "Funzionalità non attiva";
                $response['error'] = 0;
                $response['status'] = 0;
                return new JsonResponse($response);
            }

            $response['message'] = "Errore..aggiorna o contatta l'assistenza";
            $response['error'] = 1;
            $response['status'] = 0;
            return new JsonResponse($response);
        }

        if ($current[0]['command'] != AllConstants::TRK_EME_OFF) {
            $split = explode(':', $current[0]['command']);
            $time = $split[1];
            $type = 'ON';
        }

        if ($type == 'OFF') {
            if ($current[0]['stato'] == 1) {
                $this->updateTracking($steal, 0);
                $response['message'] = "Funzionalità non attiva";
                $response['error'] = 0;
                $response['status'] = 0;
                return new JsonResponse($response);
            }

            foreach ($commands as $command) {
                if ($command['command'] != AllConstants::TRK_EME_OFF) {
                    break;
                }
                if ($command['stato'] == 1) {
                    $this->updateTracking($steal, 0);
                    $response['message'] = "Funzionalità non attiva";
                    $response['error'] = 0;
                    $response['status'] = 0;
                    return new JsonResponse($response);
                }
            }
            $this->updateTracking($steal, 2);
            $response['message'] = "In attesa di disattivazione...";
        }

        if ($type == 'ON') {
            if ($current[0]['stato'] == 1) {
                $this->updateTracking($steal, 1);
                $response['message'] = "Funzionalità attiva";
                $response['error'] = 0;
                $response['status'] = 1;
                $response['time'] = $time;
                return new JsonResponse($response);
            }

            foreach ($commands as $command) {
                if ($command['command'] == AllConstants::TRK_EME_OFF || $command['command'] != AllConstants::TRK_EME . $time) {
                    break;
                }
                if ($command['stato'] == 1) {
                    $this->updateTracking($steal, 1);
                    $response['message'] = "Funzionalità attiva";
                    $response['error'] = 0;
                    $response['status'] = 1;
                    $response['time'] = $time;
                    return new JsonResponse($response);
                }
            }
            $this->updateTracking($steal, 3);
            $response['message'] = "In attesa di attivazione...";
        }

        $response['error'] = 0;
        $response['status'] = 'pending';
        $response['commandId'] = $commandId;

        return new JsonResponse($response);
    }

}