<?php

namespace AppBundle\Controller\IsaStealCenter\Handle;

use AppBundle\Exceptions\AccessStealException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Constants\AllConstants;
use AppBundle\Entity\IsaSteals;
use AppBundle\Helper\Date;
use \DateTime;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;



/**
 * @Route("/stealCenter/handle/{stealId}")
 * @ParamConverter("steal", class="AppBundle:IsaSteals", options={"id" = "stealId"})
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class IndexController extends AbstractStealController
{

    /**
     * @Route("", name="isastealcenter_handle")
     */
    public function indexAction(IsaSteals $steal)
    {
        $ongoingStatus = $this->getStealsDocManager()->getRepository('AppBundle:IsaStealsStatus')->findOneByIsaStealStatusId(1);
        $user = $this->getUser();
        if ($steal && $steal->getIsaStealStatus() == $ongoingStatus && $steal->getIsaStealUser() !== $user->getUsername()) {
            throw new AccessStealException("Non puoi accedere a questa pratica di furto perchè già in lavorazione dall'utente " . $steal->getIsaStealUser(), 409);
        }

        $this->setStealLog($steal, $this->logTypeId($steal->getIsaStealStatus()->getIsaStealStatusId()));
        $data = $this->getDataToRender($steal);
        $this->updateStatus($steal);

        return $this->render('stealCenter/handle/handle.html.twig', array(
            'event_info'        => $data['event_info'],
            'localization_info' => $data['coordinates'],
            'address'           => $data['address'],
            'steal_address'     => $data['steal_address'],
            'etracking'         => $data['etracking']
        ));
    }

    protected function getDataToRender(IsaSteals $steal) {
        $address = "---";
        $steal_address = "---";
        $coordinates = $this->getGatewayApi()->getLastLocalizationEvent($steal->getIsaStealDeviceSn());
        if (empty($coordinates) || is_numeric($coordinates)) {
            $coordinates = array('blat' => null, 'blong' => null);
        }

        $a = array(
            'lat'   => ((double)$steal->getIsaStealLat() != 0) ? (double)$steal->getIsaStealLat() : null,
            'long'  => ((double)$steal->getIsaStealLong() != 0) ? (double)$steal->getIsaStealLong() : null
        );
        $b = array(
            'lat'   => (null !== $coordinates['blat'] && 0 != (double)$coordinates['blat']) ? (double)$coordinates['blat'] : null,
            'long'  => (null !== $coordinates['blong'] && 0 != (double)$coordinates['blong']) ? (double)$coordinates['blong'] : null
        );

        if ($b['lat'] !== null && $b['long'] !== null) {
            $geo = $this->getStealGeocoderService()->reverse($b['lat'], $b['long']);
            $address = $geo['streetName'] . ' ' . $geo['streetNumber'] . ' - ' . $geo['zipcode'] . ' ' . $geo['city'];
        }

        if ($a['lat'] !== null && $a['long'] !== null) {
            $geo = $this->getStealGeocoderService()->reverse($a['lat'], $a['long']);
            $steal_address = $geo['streetName'] . ' ' . $geo['streetNumber'] . ' - ' . $geo['zipcode'] . ' ' . $geo['city'];
        }

        $data = array(
            'event_info'    => $this->getSerializer()->toArray($steal),
            'coordinates'   => $coordinates,
            'address'       => $address,
            'steal_address' => $steal_address,
            'etracking'     => $this->getEmergencyTrackingInfo($steal)
        );
        return $data;
    }

    protected function updateStatus(IsaSteals $steal) {
        if (! in_array( $steal->getIsaStealStatus()->getIsaStealStatusId(), [0, 3] ) ) {
            return;
        }

        if ($steal->getIsaStealStatus()->getIsaStealStatusId() == 1) {
            return;
        }

        $em = $this->getStealsDocManager();
        $user = $this->getUser();
        $steal->setIsaStealStatus($em->getRepository('AppBundle:IsaStealsStatus')->findOneByIsaStealStatusId(1));
        $steal->setIsaStealUser($user->getUsername());
        
        $em->persist($steal);
        $em->flush();
    }
}