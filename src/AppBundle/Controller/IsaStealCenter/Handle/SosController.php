<?php

namespace AppBundle\Controller\IsaStealCenter\Handle;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Controller\IsaStealCenter\AbstractStealController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\IsaSteals;


/**
 * @Route("/stealCenter/handle/{stealId}")
 * @ParamConverter("steal", class="AppBundle:IsaSteals", options={"id" = "stealId"})
 * @Security("has_role('ROLE_ISASTEALCENTER')")
 */
class SosController extends AbstractStealController
{
    /**
     * Dati della tabella in formato json (usato da dataTables)
     * @Route("/sos.json", name="isastealcenter_sos_json")
     */
    public function tableJsonAction(Request $request, IsaSteals $steal)
    {
        $sos = array();
        $now = new \DateTime();
        $serial = $steal->getIsaStealDeviceSn();
        $start = ($steal->getIsaStealDate()) ? $steal->getIsaStealDate()->format('Y-m-d') : $steal->getIsaStealInsertDate()->format('Y-m-d');
        $end = (null !== $steal->getIsaStealUpdateDate() && $steal->getIsaStealStatus()->getIsaStealStatusId() == 2) ? $steal->getIsaStealUpdateDate()->format('Y-m-d') : $now->format('Y-m-d');
        $data = $this->getSosEvents($serial, $start, $end, $this->getQbCriteria($request), $this->getQbPaginate($request));

        // PREVEDERE IN CASO DI ERRORE 500 UN EXCEPTION
        if (is_numeric($data) || is_null($data)) {
            $json = array(
                "recordsTotal"      => 0,
                "recordsFiltered"   => 0,
                "data"              => array()
            );
            return new JsonResponse($json);
        }

        foreach ($data as $row) {
            try {
                $geo = $this->getStealGeocoderService()->reverse($row['blat'], $row['blong']);
                $row['address'] = $geo['streetName'] . ' ' . $geo['streetNumber'] . ' - ' . $geo['zipcode'] . ' ' . $geo['city'];
            } catch (\Exception $e) {
                $row['address'] = 'Non trovato';
            }
            $sos[] = $row;
        }

        // CHIEDERE AL GATEWAY IL RISULTATO DEL COUNT COME JSON E NON COME NUMERIC
        // NECESSARIO CONTROLLO CHIAMATA NON ANDATA A BUON FINE
        $count = $this->getGatewayApi()->getCountSosEvents($serial, $start, $end);
        $json = array(
            "recordsTotal"      => $count,
            "recordsFiltered"   => $count,
            "data"              => $sos
        );
        return new JsonResponse($json);
    }

}