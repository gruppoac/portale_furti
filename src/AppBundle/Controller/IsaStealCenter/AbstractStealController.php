<?php

namespace AppBundle\Controller\IsaStealCenter;

use AppBundle\Entity\IsaSteals;
use AppBundle\Entity\IsaStealsLogTypes;
use AppBundle\Constants\AllConstants;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\ApiManager;
use AppBundle\Entity\IsaStealsLog;
use AppBundle\Entity\User;
use AppBundle\Helper\LabelStatus;
use AppBundle\Model\IsaSteal\CarReportingModel;
use AppBundle\Model\IsaSteal\HolderReportingModel;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AppBundle\Helper\DataHelper;
use Exception;
use Symfony\Component\VarDumper\Cloner\Data;

class AbstractStealController extends Controller {

    /**
     * @return ApiManager
     */
    protected function getApi() {
        return $this->get('api_portale');
    }

    /**
     * @return ApiGatewayManager
     */
    protected function getGatewayApi() {
        return $this->get('api_gateway');
    }

    /**
     * @return ApiDmsManager
     */
    protected function getDmsApi() {
        return $this->get('api_dms');
    }

    protected function getStealGeocoderService() {
        return $this->get('gmap_service');
    }

    protected function getAddress($lat, $lon) {
        return $this->get("geocoder")->getAddress($lat, $lon);
    }

    protected function getStealsInfo($stealId) {
        $em = $this->getStealsDocManager();

        $event_info = $em->createQueryBuilder()
                ->select("is")
                ->from('AppBundle:IsaSteals', 'is')
                ->where("ie.isaStealId = :stealId")
                ->setParameter('stealId', $stealId)
                ->getQuery()
                ->getArrayResult();

        return $event_info;
    }

    /**
     * @param $localizations
     * @return array
     */
    protected function getLocalizationsToJson($localizations) {
        $data = array();
        foreach($localizations as $item) {
            $data[] = $this->getLocalizationsJsonRow($item);
        }
        return $data;
    }

    /**
     * @param $localizations
     * @return array
     */
    protected function getLocalizationsJsonRow($localizations) {
        $row = array(
            'BTimeStamp'    => $localizations['BTimeStamp'],
            'BLat'          => $localizations['BLat'],
            'BLong'         => $localizations['BLong']
        );
        return $row;
    }

    public static function isaStealStatus($status) {
        return LabelStatus::isaStealStatus($status);
    }

    protected function setExceptionMessage($type, $message) {

        $session = $this->get("session");
        $session->getFlashBag()->add(
                $type, $message
        );
    }

    protected function getDocManager() {
        return $this->getDoctrine()->getManager();
    }

    protected function getStealsDocManager() {
        return $this->getDoctrine()->getManager();
    }

    protected function getStealByPlate($plate) {
        $steal = $this->getStealsDocManager()->getRepository('AppBundle:IsaSteals')->findByIsaStealCarPlate($plate);

        return $steal;
    }

    protected function getCarByPlate($plate) {
        $car = $this->getDocManager()->getRepository('AppBundle:Cars')->findOneByCarPlate($plate);

        return $car;
    }

    protected function getCarsByVoucher($voucher) {
        $cars = $this->getDocManager()->getRepository('AppBundle:Cars')->findOneByCarVoucherNumber($voucher);

        return $cars;
    }

    protected function getCarByDeviceFk($fk) {
        $car = $this->getDocManager()->getRepository('AppBundle:Cars')->findOneByCarDeviceFk($fk);

        return $car;
    }

    protected function getDeviceById($id) {
        $car = $this->getDocManager()->getRepository('AppBundle:Devices')->findOneByDeviceId($id);

        return $car;
    }

    protected function getDeviceBySn($sn) {
        $device = $this->getDocManager()->getRepository('AppBundle:Devices')->findOneByDeviceSn($sn);

        return $device;
    }

    protected function getBlackboxDb() {
        return $this->get('doctrine.dbal.blackbox_connection');
    }

    protected function getLocalizationInfo($sn) {
        $localization_info = $this->getBlackboxDb()->fetchAll("SELECT * FROM zblocalization WHERE IDBlackBox =(SELECT IDBlackBox FROM BlackBox WHERE BBSerial='" . $sn . "') ORDER BY idZBLocalization DESC LIMIT 1");

        return $localization_info;
    }

    protected function getBlackboxIdBySn($sn) {
        $blackboxId = $this->getBlackboxDb()->fetchAll("SELECT IDBlackBox FROM blackbox WHERE BBSerial ='" . $sn . "'");
        return ($blackboxId && isset($blackboxId[0])) ? $blackboxId[0]['IDBlackBox'] : false;
    }

    protected function trackingEnable($serial, $time) {
        $prepare = $this->getGatewayApi()->setCommand(array('serial' => $serial, 'command' => AllConstants::GENR_TTPD_0));
        if (is_numeric($prepare)) {
            return false;
        }
        $data = $this->getGatewayApi()->setCommand(array('serial' => $serial, 'command' => AllConstants::TRK_EME . $time));
        return (!is_numeric($data)) ? $data['idlastCommand'] : null;
    }

    protected function trackingDisable($serial) {
        $prepare = $this->getGatewayApi()->setCommand(array('serial' => $serial, 'command' => AllConstants::GENR_TTPD));
        if (is_numeric($prepare)) {
            return false;
        }
        $data = $this->getGatewayApi()->setCommand(array('serial' => $serial, 'command' => AllConstants::TRK_EME_OFF));
        return (!is_numeric($data)) ? $data['idlastCommand'] : false;
    }

    protected function getSerializer() {
        return $this->get('jms_serializer');
    }

    protected function getStealLogType($type) {
        $entity = $this->getStealsDocManager()->getRepository('AppBundle:IsaStealsLogTypes')->findByIsaStealLogTypeId($type);

        return $entity;
    }

    protected function logTypeId($status) {
        switch($status) {
            case 0:
                return 8;
            case 1:
                return 10;
            case 2:
                return 11;
            case 3:
                return 9;
            default:
                return false;
        }
    }

    protected function setStealLog(IsaSteals $steal = null, $type, $note = null) {
        $em = $this->getStealsDocManager();
        $user = $this->getUser();
        $logType = $this->getStealLogType($type);
        $isaStealsLog = new isaStealsLog();

        $isaStealsLog->setIsaStealId(($steal) ? $steal : null);
        $isaStealsLog->setIsaStealLogInsertDate(new \DateTime());
        $isaStealsLog->setIsaStealLogType($logType[0]);
        $isaStealsLog->setIsaStealLogNote(($note) ? serialize($note) : null);
        $isaStealsLog->setIsaStealLogUser($user->getUsername());

        $em->persist($isaStealsLog);
        $em->flush();
    }

    protected function getLocalizationEvents($serial, $start, $end = null, $filter = null, $paginate)
    {
        $params = array(
            'limit'     => $paginate['length'],
            'offset'    => $paginate['start']
        );
        $results = $this->getGatewayApi()->getLocalizationEvents($serial, $start, $end, $params);
        return $results;
    }

    protected function getSosEvents($serial, $start, $end = null, $filter = null, $paginate)
    {
        $params = array(
            'limit'     => $paginate['length'],
            'offset'    => $paginate['start']
        );
        $results = $this->getGatewayApi()->getSosEvents($serial, $start, $end, $params);
        return $results;
    }

    protected function getCrashEvents($serial, $start, $end = null, $filter = null, $paginate)
    {
        $params = array(
            'limit'     => $paginate['length'],
            'offset'    => $paginate['start']
        );
        $results = $this->getGatewayApi()->getSosEvents($serial, $start, $end, $params);
        return $results;
    }

    protected function getTamperEvents($serial, $start, $end = null, $filter = null, $paginate)
    {
        $params = array(
            'limit'     => $paginate['length'],
            'offset'    => $paginate['start']
        );
        $results = $this->getGatewayApi()->getTamperEvents($serial, $start, $end, $params);
        return $results;
    }

    protected function getTamper($data) {
        $tamper = array();

        if (count($data) > 0) {
            foreach ($data as $row) {
                $description = $this->getTamperLabel(substr($row['eventInfo'], 0, 2));
                $tamper[] = array(
                    'btimeStamp'    => $row['btimeStamp'],
                    'descStatoZB'   => $description,
                    'blat'          => $row['blat'],
                    'blong'         => $row['blong'],
                    'address'       =>$row['address']
                );
            }
        }
        return $tamper;
    }

    protected function getTamperLabel($str) {
        switch($str) {
            case "00":
                return "Violato";
            case "01":
                return "Regolare";
            default:
                return "Non trovato";
        }
    }

    protected function getEngineEvents($serial, $start, $end = null, $filter = null, $paginate)
    {
        $params = array(
            'limit'     => $paginate['length'],
            'offset'    => $paginate['start']
        );
        $results = $this->getGatewayApi()->getEngineEvents($serial, $start, $end, $params);
        return $results;
    }

    protected function getPowerEvents($serial, $start, $end = null, $filter = null, $paginate)
    {
        $params = array(
            'limit'     => $paginate['length'],
            'offset'    => $paginate['start']
        );
        $results = $this->getGatewayApi()->getPowerEvents($serial, $start, $end, $params);
        return $results;
    }

    protected function getOffEngineMovingEvents($serial, $start, $end = null, $filter = null, $paginate)
    {
        $params = array(
            'limit'     => $paginate['length'],
            'offset'    => $paginate['start']
        );
        $results = $this->getGatewayApi()->getMovingEvents($serial, $start, $end, $params);
        return $results;
    }

    protected function getEngine($serial, $start, $end = null, $filter = null, $paginate) {
        $data = array();
        $events = $this->getEngineEvents($serial, $start, $end, $filter, $paginate);

        foreach ($events as $event) {
            $data[] = array(
               'BTimeStamp'   => $event['BTimeStamp'],
               'description'  => LabelStatus::engineStatus($event['StatoZB'])
            );
        }

        return $data;
    }

    protected function updateTracking(IsaSteals $steal, $status) {
        $em = $this->getStealsDocManager();
        $steal->setIsaStealTracking($status);
        $em->persist($steal);
        $em->flush();
    }

    protected function getQbCriteria(Request $request)
    {
        $search_data = $request->query->get('search');
        $search = ($search_data['value']) ? $search_data['value'] . '%' : null;

        return $search;
    }

    protected function getQbPaginate(Request $request)
    {
        $length = ($request->query->get('length')) ? intval($request->query->get('length')) : 10;
        $start = ($request->query->get('start')) ? intval($request->query->get('start')) : 0;

        $order = ($request->query->get('order')) ? $request->query->get('order') : null;
        if (!$order) {
            $paginate = array(
                'column'     => null,
                'dir'        => null,
                'start'      => $start,
                'length'     => $length
            );
            return $paginate;
        }

        $column_id = ($order[0]['column'] != 0) ? $order[0]['column'] : 0;
        $columns = $request->query->get('columns');
        $column = $columns[$column_id]['data'];
        $dir = ($order[0]['dir']) ? $order[0]['dir'] : 'DESC';

        $paginate = array(
            'column'     => $column,
            'dir'        => $dir,
            'start'      => $start,
            'length'     => $length
        );

        return $paginate;
    }

    protected function validationCarResponse($car, $model, $type) {
        if (is_numeric($car)) {
            switch($car) {
                case 400:
                    $note = array(
                        "message"   => "Errore sintattico nell'inserimento della targa",
                        "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                        "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                        "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
                    );
                    $this->setStealLog(null, $type, $note);
                    throw new \Exception($note['message']);
                case 404:
                    $note = array(
                        "message"   => "Vettura non trovata con la targa richiesta",
                        "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                        "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                        "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
                    );
                    $this->setStealLog(null, $type, $note);
                    throw new \Exception($note['message']);
                case 500:
                    $note = array(
                        "message"   => "Dati vettura non al momento disponibili..Riprovare o contattare l'assistenza",
                        "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                        "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                        "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
                    );
                    $this->setStealLog(null, $type, $note);
                    throw new \Exception($note['message']);
                default:
                    $note = array(
                        "message"   => "Errore interno..riprovare o contattare l'assistenza",
                        "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                        "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                        "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
                    );
                    $this->setStealLog(null, $type, $note);
                    throw new \Exception($note['message']);
            }

        } else {
            return $car;
        }
    }

    protected function validationDeviceResponse($device, $model, $type) {
        if (is_numeric($device)) {
            switch($device) {
                case 400:
                    $note = array(
                        "message"   => "Impossibile reperire le informazioni del dispositivo. Contattare l'assistenza",
                        "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                        "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                        "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
                    );
                    $this->setStealLog(null, $type, $note);
                    throw new \Exception($note['message']);
                case 404:
                    $note = array(
                        "message"   => "Dispositivo non trovato. Contattare l'assistenza",
                        "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                        "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                        "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
                    );
                    $this->setStealLog(null, $type, $note);
                    throw new \Exception($note['message']);
                case 500:
                    $note = array(
                        "message"   => "Dati seriale non al momento disponibili..Riprovare o contattare l'assistenza",
                        "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                        "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                        "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
                    );
                    $this->setStealLog(null, $type, $note);
                    throw new \Exception($note['message']);
                default:
                    $note = array(
                        "message"   => "Errore interno..riprovare o contattare l'assistenza",
                        "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                        "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                        "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
                    );
                    $this->setStealLog(null, $type, $note);
                    throw new \Exception($note['message']);
            }

        } else {
            return $device;
        }
    }

    protected function hasSteal($model) {
        $steal = $this->getStealByPlate($model->getPlate());
        if ($steal) {
            $note = array(
                "message"   => "Esiste un evento di furto per questa targa!",
                "plate"     => ($model->getPlate()) ? $model->getPlate() : null,
                "type"      => ($model->getTipo()) ? $model->getTipo()->getIsaStealTypeId() : null,
                "date"      => ($model->getDate()) ? $model->getDate()->format("Y-m-d H:i:s") : null
            );
            $this->setStealLog(null, 13, $note);
            throw new \Exception($note['message']);
        } else {
            return false;
        }
    }

    protected function getEmergencyTrackingInfo(IsaSteals $steal) {
        $type = 'OFF';
        $time = null;
        $message = "";
        $commands = $this->getGatewayApi()->getLastCommands($steal->getIsaStealDeviceSn(), AllConstants::TRK_EME, 10);
        $current = $this->getGatewayApi()->getLastCommands($steal->getIsaStealDeviceSn(), AllConstants::TRK_EME, 1);

        if (is_numeric($commands) || is_numeric($current) || is_null($commands) || is_null($current)) {
            if ($commands == 404 || $current == 404 || is_null($commands) || is_null($current)) {
                $response = array(
                    'type'      => $type,
                    'message'   => "Funzionalità non attiva",
                    'error'     => 0,
                    'status'    => 0,
                    'time'      => null,
                    'nodata'    => 1
                );
                return $response;
            }

            $response = array(
                'type'      => $type,
                'message'   => "Errore..aggiorna o contatta l'assistenza",
                'error'     => 1,
                'status'    => 0,
                'time'      => null,
                'nodata'    => 0
            );
            return $response;
        }

        if ($current[0]['command'] != AllConstants::TRK_EME_OFF) {
            $split = explode(':', $current[0]['command']);
            $time = isset($split[1]) ? $split[1] : null;
            $type = 'ON';
        }

        if ($type == 'OFF') {
            if ($current[0]['stato'] == 1) {
                $this->updateTracking($steal, 0);
                $response = array(
                    'type'      => $type,
                    'message'   => "Funzionalità non attiva",
                    'error'     => 0,
                    'status'    => 1,
                    'time'      => null,
                    'nodata'    => 0
                );
                return $response;
            }

            foreach ($commands as $command) {
                if ($command['command'] != AllConstants::TRK_EME_OFF) {
                    break;
                }
                if ($command['stato'] == 1) {
                    $this->updateTracking($steal, 0);
                    $response = array(
                        'type'      => $type,
                        'message'   => "Funzionalità non attiva",
                        'error'     => 0,
                        'status'    => 1,
                        'time'      => null,
                        'nodata'    => 0
                    );

                    return $response;
                }
            }
            $this->updateTracking($steal, 2);
            $message = "In attesa di disattivazione...";
        }

        if ($type == 'ON') {
            if ($current[0]['stato'] == 1) {
                $this->updateTracking($steal, 1);
                $response = array(
                    'type'      => $type,
                    'message'   => "Funzionalità attiva",
                    'error'     => 0,
                    'status'    => 1,
                    'time'      => $time,
                    'nodata'    => 0
                );
                return $response;
            }

            foreach ($commands as $command) {
                if ($command['command'] == AllConstants::TRK_EME_OFF || $command['command'] != AllConstants::TRK_EME . $time) {
                    break;
                }
                if ($command['stato'] == 1) {
                    $this->updateTracking($steal, 1);
                    $response = array(
                        'type'      => $type,
                        'message'   => "Funzionalità attiva",
                        'error'     => 0,
                        'status'    => 1,
                        'time'      => $time,
                        'nodata'    => 0
                    );
                    return $response;
                }
            }
            $this->updateTracking($steal, 3);
            $message = "In attesa di attivazione...";
        }

        $response = array(
            'type'      => $type,
            'message'   => $message,
            'error'     => 0,
            'status'    => 0,
            'time'      => (isset($time)) ? $time : null,
            'nodata'    => 0
        );

        return $response;
    }

    public function getReport($label) {
        $csv = $this->getParameter("csv_directory").'/steals/report_' . $label . '.csv';
        $fs = new Filesystem;
        if( ! $fs->exists($csv) ) {
            throw new \Exception("Il file non é stato ancora generato");
        }

        $response = new StreamedResponse();
        $response->setCallback(function () use ($csv) {
            echo file_get_contents($csv);
        });

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'report_' . $label . '.csv'
        );
        $response->headers->set('Content-Type', 'text/plain');
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }

}
