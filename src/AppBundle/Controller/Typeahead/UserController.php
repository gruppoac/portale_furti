<?php

namespace AppBundle\Controller\Typeahead;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/typeahead/user")
 */
class UserController extends Controller {

    /**
     * @Route("/admin/{search}", name="admin_user_typeahead")
     */
    public function indexAction(Request $request, $search) {

        $users = $this->getUsers($search);
        $json = array();
        foreach($users as $row) {
            $json[] = array( "value" => $row['email'], "label" => $row['username'] );            
        }
        return new JsonResponse($json);
    }

    
    public function getUsers($search) {
        $qb = $this->getDoctrine()->getRepository('AppBundle:User')->createQueryBuilder('u');
        $qb
            ->select('u.username, u.id, u.email')
            ->setMaxResults(30)
            ->andWhere('u.enabled = 1')
            ->andWhere('u.username LIKE :search OR u.email LIKE :search')->setParameter('search', "%".$search."%");

        return $qb->getQuery()->getArrayResult();
    }
     
}