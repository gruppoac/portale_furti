<?php

namespace AppBundle\Controller\Admin\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Exception;
use AppBundle\Form\Admin\User\AdminUserSearchType;
use AppBundle\Model\UserSearch;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


/**
 * @Route("/admin/user")
 */
class IndexController extends Controller {

    /**
     * @Route("/", name="admin_user")
     */
    public function indexAction(Request $request) {
        return $this->render('admin/user/index.html.twig', array());
    }

    /**
     * @Route("/list", name="admin_user_list")
     */
    public function listAction(Request $request) {
        
        $start = $request->query->get('start', 0);
        $limit = 20; //$request->query->get('offset', 20);
        
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->createQueryBuilder('u');
        $qb->orderBy('u.id', 'DESC')
                ->setFirstResult($start)
                ->setMaxResults($limit);
        $users = $qb->getQuery()->getArrayResult();
                
        
        return $this->render('admin/user/list.html.twig', array(            
            'users' => $users
        ));
    }
    
    
}