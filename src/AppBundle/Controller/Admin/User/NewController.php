<?php

namespace AppBundle\Controller\Admin\User;

use AppBundle\Entity\User;
use AppBundle\Helper\UserHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Form\Admin\User\AdminUserEditType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use FOS\UserBundle\Model\UserManagerInterface;

/**
 * @Route("/admin/user/new")
 */
class NewController extends Controller {

    /**
     * @Route("", name="admin_user_create")
     */
    public function indexAction(Request $request) {
        $isCurrentUserRoot = false;
        $hasUserFormRootRole = false;
        $userTypeList = UserHelper::typeList();
        $userRoleList = UserHelper::roleList();

        $currentUser = $this->getUser();
        $currentUserRoles = $currentUser->getRoles();
        foreach ($currentUserRoles as $currentUserRole) {
            if ($currentUserRole == 'ROLE_SUPER_ADMIN') {
                $isCurrentUserRoot = true;
                continue;
            }
        }

        $user = $this->getUserManager()->createUser();
        $formUrl = $this->generateUrl('admin_user_create');
        $form = $this->createForm(AdminUserEditType::class, $user, array('action' => $formUrl));
        $form->add('submit', SubmitType::class);
        
        $form->handleRequest($request);
        if($form->isValid()) {
            $userRoles =  $user->getRoles();
            foreach ($userRoles as $userRole) {
                if ($userRole == 'ROLE_SUPER_ADMIN') {
                    $hasUserFormRootRole = true;
                    continue;
                }
            }

            $hasUserFormRootType = ($user->getUserType() == User::ROOT) ?  true : false;
            if (!$isCurrentUserRoot) {
                $this->addFlash('notice', 'Non hai i permessi necessari per modificare i dati di questo utente!');
                return $this->redirect( $formUrl );
            }

            if (!$hasUserFormRootType && $hasUserFormRootRole) {
                $this->addFlash('notice', 'Non puoi assegnare ad un utente di tipo diverso da ' . $userTypeList[User::ROOT] . ' un ruolo diverso di ' . $userRoleList['ROLE_SUPER_ADMIN'] . '. Controlla i dati del form e riprova');
                return $this->redirect( $formUrl );
            }

            if ($hasUserFormRootType && !$hasUserFormRootRole) {
                $this->addFlash('notice', 'Non puoi assegnare ad un utente di tipo diverso ' . $userTypeList[User::ROOT] . ' un ruolo diverso da ' . $userRoleList['ROLE_SUPER_ADMIN'] . '. Controlla i dati del form e riprova');
                return $this->redirect( $formUrl );
            }

            ($user->getPlainPassword()) ? $user->setRecover($user->getPlainPassword()) : null;
            $this->getUserManager()->updateUser($user);
            $this->addFlash('success', 'Dati utente cambiati correttamente!');            
            $url = $this->generateUrl('admin_user_edit', array('userId'=> $user->getId() ) );
            return $this->redirect( $url );
        } 
        
        return $this->render('admin/user/new/form.html.twig', array(
            'user' => $user,
            'form' => $form->createView()
        ));

    }
    
    
    /**
     * @return UserManagerInterface
     */            
    protected function getUserManager() {
        return $this->get('fos_user.user_manager');
    }
}