<?php

namespace AppBundle\Controller\Admin\User;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\Admin\User\AdminUserSearchType;
use AppBundle\Model\UserSearch;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin/user/search")
 */
class SearchController extends Controller {

    /**
     * @Route("", name="admin_user_search_form")
     * @Method("GET")
     */
    public function blockAction(Request $request) {

        $form = $this->getSearchForm();
        return $this->render('admin/user/search/form.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("", name="admin_user_search")
     * @Method("POST")
     */
    public function searchAction(Request $request) {

        $form = $this->getSearchForm();        
        $form->handleRequest($request);
        if($form->isValid()) {         
            $model = $form->getData();
            return $this->redirectToRoute('admin_user_edit', array('userId' => $model->getUser()->getId() ));
        }
        
        return $this->render('admin/user/search/index.html.twig', array(
            'form' => $form->createView()
        ));
    }    
    
    protected function getSearchForm() {
        $model = new UserSearch();
        $formUrl = $this->generateUrl('admin_user_search');
        $form = $this->createForm(AdminUserSearchType::class, $model, array('action' => $formUrl));
        $form->add('submit', SubmitType::class);  
        return $form;
    }

}