<?php

namespace AppBundle\Controller\Admin\User;

use AppBundle\Entity\User;
use AppBundle\Helper\UserHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Form\Admin\User\AdminUserEditType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Mailer\MailerManager;

/**
 * @Route("/admin/user/info/{userId}")
 */
class EditController extends Controller {

    /**
     * @Route("/edit", name="admin_user_edit")
     * @ParamConverter("user", class="AppBundle:User", options={"id" = "userId"})
     */
    public function editAction(Request $request, UserInterface $user) {
        $isCurrentUserRoot = false;
        $hasUserFormRootRole = false;
        $userTypeList = UserHelper::typeList();
        $userRoleList = UserHelper::roleList();

        $currentUser = $this->getUser();
        $currentUserRoles = $currentUser->getRoles();
        foreach ($currentUserRoles as $currentUserRole) {
            if ($currentUserRole == 'ROLE_SUPER_ADMIN') {
                $isCurrentUserRoot = true;
                continue;
            }
        }

        $formUrl = $this->generateUrl('admin_user_edit', array('userId'=> $user->getId() ) );
        $form = $this->createForm(AdminUserEditType::class, $user, array('action' => $formUrl));
        $form->add('submit', SubmitType::class);
        
        $form->handleRequest($request);
        if($form->isValid()) {

            $userRoles =  $user->getRoles();
            foreach ($userRoles as $userRole) {
                if ($userRole == 'ROLE_SUPER_ADMIN') {
                    $hasUserFormRootRole = true;
                    continue;
                }
            }

            $hasUserFormRootType = ($user->getUserType() == User::ROOT) ?  true : false;
            if (!$isCurrentUserRoot) {
                $this->addFlash('notice', 'Non hai i permessi necessari per modificare i dati di questo utente!');
                return $this->redirect( $formUrl );
            }

            if (!$hasUserFormRootType && $hasUserFormRootRole) {
                $this->addFlash('notice', 'Non puoi assegnare ad un utente di tipo diverso da ' . $userTypeList[User::ROOT] . ' un ruolo diverso di ' . $userRoleList['ROLE_SUPER_ADMIN'] . '. Controlla i dati del form e riprova');
                return $this->redirect( $formUrl );
            }

            if ($hasUserFormRootType && !$hasUserFormRootRole) {
                $this->addFlash('notice', 'Non puoi assegnare ad un utente di tipo diverso ' . $userTypeList[User::ROOT] . ' un ruolo diverso da ' . $userRoleList['ROLE_SUPER_ADMIN'] . '. Controlla i dati del form e riprova');
                return $this->redirect( $formUrl );
            }

            ($user->getPlainPassword()) ? $user->setRecover($user->getPlainPassword()) : null;
            $userManager = $this->getUserManager();
            $userManager->updateUser($user);
            $this->addFlash('success', 'Dati utente cambiati correttamente!');

            return $this->redirect( $formUrl );
        }
        
        return $this->render('admin/user/edit/form.html.twig', array(
            'user'                  => $user,
            'form'                  => $form->createView()
        ));

    }
    
    /**
     * @Route("/recover", name="admin_user_edit_recover")
     * @ParamConverter("user", class="AppBundle:User", options={"id" = "userId"})
     */
    public function recoverAction(Request $request, UserInterface $user) {
        
        $recover = $user->getRecover(); //generate new pwd random now
        $user->setPlainPassword(  $recover );
        $this->getMailer()->sendImportUserEmailMessage($user);            
        $user->setEmailSent(true);
        $this->getUserManager()->updateUser($user);
        
        $this->addFlash('success', 'Nuovi dati inviati per email');            
        return $this->redirectToRoute('admin_user_edit', array('userId' => $user->getId() ) );

    }
       
    /**
     * @Route("/delete", name="admin_user_edit_delete")
     * @ParamConverter("user", class="AppBundle:User", options={"id" = "userId"})
     */
    public function deleteAction(Request $request, UserInterface $user) {
        
        //$this->getUserManager()->deleteUser($user); 
        $user->setEnabled(false);
        $this->getUserManager()->updateUser($user);
        $this->addFlash('success', 'Utente rimosso correttamente dal sistema!');            
        return $this->redirectToRoute('admin_user');

    }    
    
    
    protected function getUserWorkshop($userId) {
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:UserWorkshop')->createQueryBuilder('uw');
        $result = $qb->addSelect('w')
//                ->join('uw.workshop', 'w')
                ->where('uw.user = :userId')->setParameter('userId', $userId)
                ->getQuery()->getResult();
        foreach($result as $uw) {
            return $uw;
        }
        return;
    }
    
    /**
     * @return UserManagerInterface
     */            
    protected function getUserManager() {
        return $this->get('fos_user.user_manager');
    }
    
    /**
     * 
     * @return MailerService
     */
    protected function getMailer() {
        return $this->get('mailer_manager');
    }
}