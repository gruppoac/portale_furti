<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\IsaStealsLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Exception;
use DateTime;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        try {
            $route = $this->getUserRoute($this->getUser());
            return $this->redirectToRoute($route);        
        } catch (Exception $e) {
            // Change this code to logout and redirect user to login page!!
            return $this->render('default/index.html.twig');
        }
    }

    /**
     * Ritorna la route prefefinita in base al ruolo
     * @return string
     * @throws \Exception
     */
    public function getUserRoute(UserInterface $user) {
                
        /*if ($this->isGranted('ROLE_MANAGEMENT')) {
            return 'management_profile';
        }
        if ($this->isGranted('ROLE_WORKSHOP')) {
            return 'workshop_profile';
        }
        if ($this->isGranted('ROLE_HELPDESK')) {
            return 'helpdesk_profile';
        }
        if ($this->isGranted('ROLE_ISAOPERATIONCENTER')) {
            return 'isa_operation_center_event_list';
        }*/
        if ($this->isGranted('ROLE_ISASTEALCENTER')) {
            $this->LoginAudit();
            return 'isa_steal_center_list_pending';
        }

        throw new Exception("Non ha permessi particolari");
    }

    protected function LoginAudit() {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $logType = $em->getRepository('AppBundle:IsaStealsLogTypes')->findByIsaStealLogTypeId(15);
        $isaStealsLog = new isaStealsLog();

        $isaStealsLog->setIsaStealId(null);
        $isaStealsLog->setIsaStealLogInsertDate(new \DateTime());
        $isaStealsLog->setIsaStealLogType($logType[0]);
        $isaStealsLog->setIsaStealLogNote(null);
        $isaStealsLog->setIsaStealLogUser($user->getUsername());

        $em->persist($isaStealsLog);
        $em->flush();
    }

}