<?php

namespace AppBundle\Form\IsaSteal;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\IsaSteal\ReportingType;

/**
 * Form di ricerca per targa
 * Estende ReportingType perchè questo oggetto cambia se cambia l'oggetto Reporting di input
 */
class CarReportingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('plate', TextType::class);
        
    }
    
    public function getParent() {
        return ReportingType::class;
    }
    
    
    public function getName() {
        return "steal_car_reporting_type";
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\IsaSteal\CarReportingModel',
        ));
    }       
}
