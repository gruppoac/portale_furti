<?php

namespace AppBundle\Form\IsaSteal;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\IsaSteal\ReportingType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

/**
 * Form di ricerca per nome e cognome cliente
 * Estende ReportingType perchè questo oggetto cambia se cambia l'oggetto Reporting di input
 */
class HolderReportingType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', TextType::class, array('label' => 'Nome'))
            ->add('surname', TextType::class, array('label' => 'Cognome'))
            /*->add('plate', ChoiceType::class, array(
                'choices' => array(
                    'select car' => ''
                )
            ));*/
        ;

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $formData = $form->getData();
                if ($formData->getPlate() ) {
                    $selectedCar = array(
                        $formData->getPlate() => $formData->getPlate()
                    );
                    $form->add('plate', ChoiceType::class, array(
                        'allow_extra_fields' => true,
                        'choices' => $selectedCar
                    ));
                }
            }
        );
    }

    public function getParent() {
        return ReportingType::class;
    }

    public function getName() {
        return "steal_holder_reporting_type";
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\IsaSteal\HolderReportingModel',
        ));
    }
}
