<?php

namespace AppBundle\Form\IsaSteal;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\Type\DatepickerType;

class ReportingType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tipo', EntityType::class, array(
                'class' => 'AppBundle:IsaStealsTypes',
                'choice_label' => 'isaStealTypeDescription',
                'placeholder' => 'Seleziona il tipo di furto',
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->andWhere("t.isaStealTypeId IN (:status)")->setParameter('status', array(1, 2))
                        ->orderBy('t.isaStealTypeDescription', 'ASC');
                },
            ))
            ->add('complaint', TextType::class, array('required' => false))
            ->add('date', DatepickerType::class)
            ->add('submit', SubmitType::class);

    }   
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\IsaSteal\ReportingModel'
        ));
    }
    
    public function getName() {
        return 'steal_reporting_type';
    }
    
}
