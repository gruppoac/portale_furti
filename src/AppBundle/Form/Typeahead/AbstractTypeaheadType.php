<?php

namespace AppBundle\Form\Typeahead;

use Symfony\Component\Form\AbstractType;
//use Symfony\Component\Form\FormBuilderInterface;
//use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Form\Type\TypeaheadType;

use Doctrine\Common\Persistence\ObjectManager;

abstract class AbstractTypeaheadType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    protected $om;

    public function setObjectManager(ObjectManager $om) {
        $this->om = $om;
    }
    

    public function getParent()
    {
        return TypeaheadType::class;
    }
    
    public function getName()
    {
        return 'abstract_typeahead_type';
    }        
    
}
