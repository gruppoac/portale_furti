<?php

namespace AppBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\Type\DatepickerType;

/**
 * Form di date range
 */
class DateRangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', DatepickerType::class, array('label' => 'Data di inizio', 'required'=> false))
            ->add('end', DatepickerType::class, array('label' => 'Data di fine', 'required'=> false))
        ;

    }

    public function getName() {
        return "date_range_type";
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\Filter\DateRangeModel',
        ));
    }
}
