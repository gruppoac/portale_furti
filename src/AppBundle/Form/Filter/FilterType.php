<?php

namespace AppBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\Filter\FilterModelType;
use AppBundle\Form\Filter\DateRangeType;

/**
 * Form di default per i filtri di ricerca
 * Estende FilterModelType perchè questo oggetto cambia se cambia l'oggetto Filter di input
 */
class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('query', TextType::class, array('required' => false ))
            ->add('search', SubmitType::class)
            ->add('dateRange', DateRangeType::class)
        ;
        
    }
    
    public function getParent() {
        return FilterModelType::class;
    }
    
    
    public function getName() {
        return "filter_type";
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\Filter\FilterModel',
        ));
    }       
}
