<?php

namespace AppBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Model\Filter\SortModel;

/**
 * Form per la selezione dell'ordinamento (utilizzato nel componente Table)
 */
class SortType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('order', TextType::class, array( 'required'=> false, 
                                                    'attr' => array(
                                                        'data-table' => 'sort-input'
                                                    )
            ))
            ->add('type', ChoiceType::class, array(
                'choices' => $this->getChoices(),
                'required' => false,
                'attr' => array(
                    'data-table' => 'sort-type-input'
                )
            ) )
        ;
    }
    
    protected function getChoices() {
        return array(
            'Crescente' => SortModel::ASC,
            'Descrescente' => SortModel::DESC ,
        );
    }
    
    public function getName() {
        return "sort_type";
    }
    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\Filter\SortModel',
        ));
    }       
}
