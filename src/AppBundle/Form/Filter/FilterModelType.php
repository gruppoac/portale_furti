<?php

namespace AppBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\Filter\SortType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Modello base per i form di ricerca, include ordinamento e paginazione
 */
class FilterModelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sort', SortType::class)
            ->add('page', NumberType::class, array(
                'required' => true,
                'attr' => array(
                    'data-table' => 'page-input'
                )
            ))
            ->add('limit', ChoiceType::class, array(
                'required' => true ,
                'attr' => array(
                    'data-table' => 'limit-input'
                ),
                'choices' => $this->getLimitChoices()
            ))
        ;
    }
    
    
    protected function getLimitChoices() {
        return array(
            5 => 5,
            10 => 10,
            25 => 25,
            50 => 50,
            100 => 100,
        );
    }    
    
    public function getName() {
        return "filtermodel_type";
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\Filter\FilterModel',
        ));
    }       
}
