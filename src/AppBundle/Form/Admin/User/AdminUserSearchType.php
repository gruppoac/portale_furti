<?php

namespace AppBundle\Form\Admin\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Form\User\UserSearchType;
use AppBundle\Form\DataTransformer\UserTransformer;

class AdminUserSearchType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $builder->add('user', UserSearchType::class);
    }   
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Model\UserSearch'
        ));
    }
    
    public function getName() {
        return 'user_search_type';
    }
    
    
}
