<?php

namespace AppBundle\Form\Admin\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Form\User\UserRolesType;
use AppBundle\Form\User\UserTypeType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class AdminUserEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('userType', UserTypeType::class, array('required' => true ));
        $builder->add('roles', UserRolesType::class, array('required' => true ));
        
        $builder->add('username', TextType::class, array('required' => true ));
        $builder->add('email', EmailType::class, array('required' => true ));

        $builder->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class, //PasswordType::class,
                'invalid_message' => 'Le password non combaciano',
                'required' => false,
                'first_options'  => array('label' => 'Nuova Password'),
                'second_options' => array('label' => 'Conferma Password'),
        ));
        $builder->add('emailSent', CheckboxType::class, array('required' => false ));
        $builder->add('enabled', CheckboxType::class, array('required' => false ));
        //roles
        //altro
    }   
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'validation_groups' => array('Profile'),
        ));
    }
    
    public function getName() {
        return 'user_search_type';
    }
    
    
}
