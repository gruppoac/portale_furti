<?php

namespace AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Helper\UserHelper;

class UserTypeType extends AbstractType
{

    public function getParent()
    {
        return ChoiceType::class;
    }
    
    public function getName() {
        return 'usertype_type';
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => UserHelper::typeSelectList(),
            'multiple' => false,
            'attr' => array('class' => 'dmsSelect2')
        ));
    }    
    
}
