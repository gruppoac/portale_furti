<?php

namespace AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\UserBundle\Model\UserManagerInterface;
//use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\DataTransformer\UserTransformer;
use AppBundle\Form\Type\TypeaheadType;

class UserSearchType extends AbstractType
{
    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    public function setUserManager(UserManagerInterface $userManager) {
        $this->userManager = $userManager;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new UserTransformer($this->userManager);
        $builder->addModelTransformer($transformer);
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'invalid_message' => 'The selected user does not exist',
            'route' => 'admin_user_typeahead'
        ));
    }

    public function getParent()
    {
        return TypeaheadType::class;
    }
    
    public function getName()
    {
        return 'user_search_type';
    }    
    
}
