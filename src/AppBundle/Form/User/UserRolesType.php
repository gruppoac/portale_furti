<?php

namespace AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Helper\UserHelper;

class UserRolesType extends AbstractType
{

    public function getParent()
    {
        return ChoiceType::class;
    }
    
    public function getName() {
        return 'user_role_type';
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => UserHelper::roleSelectList(),
            'multiple' => true,
            'attr' => array('class' => 'dmsSelect2')
        ));
    }    
    
}
