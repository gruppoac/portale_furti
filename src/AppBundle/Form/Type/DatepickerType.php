<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class DatepickerType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['date_format'] = $options['datepicker_format']; 
    }

    public function getParent()
    {
        return DateType::class;
    }
    
    public function getName() {
        return 'datepicker_type';
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'widget' => 'single_text',
            'html5' => false,
            'format' => 'dd/MM/y',
            'datepicker_format' => 'dd/mm/yyyy'
        ));
    }    
    
}
