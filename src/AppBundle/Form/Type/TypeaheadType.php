<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class TypeaheadType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    protected $router;

    public function setRouter(RouterInterface $router) {
        $this->router = $router;
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['url']  = $this->router->generate($options['route'], array_merge( array('search' => $options['wildcard']), $options['routeParams']) );
        $view->vars['wildcard'] = $options['wildcard'];
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)    {
        $builder->add('value', HiddenType::class, array('required' => false, 'attr' => array('data-typeahead' => 'value')  ) )
                ->add('label', TextType::class, array('required' => false, 'attr' => array('data-typeahead' => 'label') ) )
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {   
        $resolver->setRequired('route');
        $resolver->setDefaults(array(
            'routeParams' => array(),
            'wildcard' => 'q'
        ));
    }

    
    public function getName() {
        return 'typeahead_type';
    }
    
    
}
