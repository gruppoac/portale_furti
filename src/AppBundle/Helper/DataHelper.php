<?php

namespace AppBundle\Helper;


class DataHelper {

    /**
     * Hexadecimal to string
     *
     * @param string $hex
     * @return string
     */
    function hex2str($hex) {
        $p = pack("H*", str_replace(array("\r", "\n", " "), "", $hex));
        return $p;
    }

    /**
     * String to hexadecimal
     *
     * @param string $str
     * @return string
     */
    function str2hex($str) {
        return current(unpack("H*", $str));
    }

}
