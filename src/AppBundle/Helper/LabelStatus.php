<?php

namespace AppBundle\Helper;


class LabelStatus {

    public static function getDeviceStatus($status) {
        switch($status) {
            case 0:
                return "Libero";
            case 1:
                return "???";
            case 2:
                return "Installato";
            case 3:
                return "Disinstallato";
            case 4:
                return "Guasto";
            case 5:
                return "Anomalia";
            case 6:
                return "Recuperato";
            case 9:
                return "Sconosciuto";
            default:
                return "---";
        }
    }

    public static function getDeviceStatusByLabel($label) {
        switch($label) {
            case 'Libero':
                return 0;
            case "Installato":
                return 2;
            case "Disinstallato":
                return 3;
            case "Guasto":
                return 4;
            case "Anomalia":
                return 5;
            case "Recuperato":
                return 6;
            case "Sconosciuto":
                return 9;
            default:
                return null;
        }
    }

    public static function getCarStatus($status) {
        switch($status) {
            case 0:
                return "Da prenotare";
            case 1:
                return "Da installare";
            case 2:
                return "Installata";
            case 3:
                return "Da disinstallare";
            case 4:
                return "Da sostituire";
            case 5:
                return "Da manutenere";
            case 6:
                return "Disinstallata";
            default:
                return "---";
        }
    }

    public static function friendlyCarStatus($status) {
        switch($status) {
            case 0:
                return "In attesa di prenotazione";
            case 1:
                return "In attesa di installazione";
            case 2:
                return "Installata";
            case 3:
                return "In attesa di disinstallazione";
            case 4:
                return "In attesa di disinstallazione per cambio veicolo";
            case 5:
                return "In attesa di disinstallazione per sostituzione dispositivo";
            case 6:
                return "Disinstallata";
            default:
                return "---";
        }
    }

    public static function getSimStatus($status) {
        switch($status) {
            case 0:
                return "Non attiva";
            case 1:
                return "Attiva";
            default:
                return "---";
        }
    }

    public static function isaStealType($type)
    {
        switch($type) {
            case 32:
                return "Furto Dispositivo";
            case 33:
                return "Furto Vettura";
            default:
                return "---";
        }
    }
    
    public static function isaStealStatus($status)
    {
        switch($status) {
            case 0:
                return "Da lavorare";
            case 1:
                return "In lavorazione";
            case 2:
                return "Chiuso";
            case 3:
                return "In attesa";
            case 4:
                return "Chiuso e confermato";
            default:
                return "---";
        }
    }    

    public static function engineStatus($status)
    {
        switch ($status) {
            case 1:
                return 'Quadro spento';
            case 3:
                return 'Quadro acceso';
        }
    }

    public static function isaStealLabelTracking($status) {
        switch ($status) {
            case 0:
                return "Disattivo";
            case 1:
                return "Attivo";
            case 2:
                return "In disattivazione";
            case 3:
                return "In attivazione";
            case 4:
                return "Non disponibile";
            default:
                return "Non disponibile";
        }
    }
}

