<?php

namespace AppBundle\Helper;


class SerializerHelper {

    public static function unserialize($str) {
        if(!$str) {
            return false;
        }
        $data = unserialize($str);
        return $data;
    }

    public static function serialize($arr) {
        if(!$arr) {
            return false;
        }
        $str = serialize($arr);
        return $str;
    }

}
