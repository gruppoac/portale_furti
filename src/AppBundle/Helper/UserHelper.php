<?php

namespace AppBundle\Helper;
use AppBundle\Entity\User;

class UserHelper {
    
    public static function getUserType($code) {
        $types = self::typeList();
        return array_key_exists($code, $types) ? $types[$code] : null;
    }


    public static function typeList() {

        return array(
            User::ROOT      => 'ROOT',
            User::FURTI     => 'FURTI'

        );
        
    }    

    public static function roleSelectList() {
        return self::invertArray(self::roleList());
    }
    
    public static function typeSelectList() {
        return self::invertArray( self::typeList() );
    }
    
    
    public static function roleList() {

        return array(
            'ROLE_SUPER_ADMIN'          => 'ROOT',
            'ROLE_ISASTEALCENTER'       => 'OPERATORE FURTI',
            'ROLE_ADMIN_ISASTEALCENTER' => 'ADMIN FURTI',
        );
        
    }

    public static function invertArray($list) {
        $choices = array();
        foreach($list as $k => $v) {
            $choices[$v] = $k;
        }
        return $choices;        
    }
    


}
