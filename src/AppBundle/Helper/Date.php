<?php

namespace AppBundle\Helper;

use DateTimeZone;
use DateTime;

class Date {

    public static function gmtData($data = null, $format = "d/m/Y", $gmt = null) {

        if(!$data) {
            return "---";
        }
        if(!$data instanceOf DateTime ) {
            $data = new DateTime($data);
        }
        if($gmt) {
            $data->setTimezone(new DateTimeZone($gmt));
        }
        return $data->format($format);
    }

    public static function gmtDataTime($data = null, $format = "d/m/Y H:i:s", $gmt = null ) {
        return self::gmtData($data, $format, $gmt);
    }

}
