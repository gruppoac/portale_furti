<?php

namespace AppBundle\Helper;
use Symfony\Component\Filesystem\Filesystem;


class CsvReader {
        

    /**
     * Legge il file CSV in input e ritorna un array di dati
     * @param string $uri path del file csv
     * @return array 
     * @throws \Exception
     */
    public static function readCsv($uri) {
        $fs = new Filesystem;

        if (!$fs->exists($uri)) {
            throw new \Exception("File non trovato");
        }
        if (($handle = fopen($uri, 'r')) === FALSE) {
            throw new \Exception("File non trovato");
        }

        $data = array();
        while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $data[] = $row;
        }
        fclose($handle);
        return $data;
    }    
    
    
}