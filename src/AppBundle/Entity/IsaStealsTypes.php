<?php
// src/AppBundle/Entity/IsaStealsTypes.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\IsaStealsTypes
 *
 * @ORM\Table(name="isa_steals_types")
 * @ORM\Entity
 *
 */
class IsaStealsTypes
{
    /**
     * @var integer $isaStealTypeId
     *
     * @ORM\Column(name="isa_steal_type_id", type="bigint", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $isaStealTypeId;

    /**
     * @var string $isaStealTypeDescription
     *
     * @ORM\Column(name="isa_steal_type_description", type="string", length=256, nullable=false)
     */
    private $isaStealTypeDescription = NULL;



    /**
     * Get isaStealTypeId
     *
     * @return integer
     */
    public function getIsaStealTypeId()
    {
        return $this->isaStealTypeId;
    }

    /**
     * Set isaStealTypeDescription
     *
     * @param string $isaStealTypeDescription
     *
     * @return IsaStealsTypes
     */
    public function setIsaStealTypeDescription($isaStealTypeDescription)
    {
        $this->isaStealTypeDescription = $isaStealTypeDescription;

        return $this;
    }

    /**
     * Get isaStealTypeDescription
     *
     * @return string
     */
    public function getIsaStealTypeDescription()
    {
        return $this->isaStealTypeDescription;
    }

    function __toString()
    {
        return $this->isaStealTypeDescription;
    }


}
