<?php
// src/AppBundle/Entity/IsaSteals.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
//use Doctrine\Common\Collections\ArrayCollection;

/**
 * AppBundle\Entity\IsaSteals
 *
 * @ORM\Table(name="isa_steals", indexes={
 *      @ORM\Index(name="plate_idx", columns={"isa_steal_car_plate"}),
 *      @ORM\Index(name="device_idx", columns={"isa_steal_device_sn"}),
 *      @ORM\Index(name="status_idx", columns={"isa_steal_status"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\IsaStealsRepository")
 * @UniqueEntity("car_plate")
 */
class IsaSteals
{
    /**
     * @var integer $isaStealId
     * @ORM\Column(name="isa_steal_id", type="bigint", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $isaStealId;

    /**
     * @var string $isaStealCarPlate
     *
     * @ORM\Column(name="isa_steal_car_plate", type="string", length=10, nullable=false)
     * @Assert\Length(
     *      min = 7,
     *      max = 10,
     *      minMessage = "La targa deve avere un minimo di {{ limit }} caratteri",
     *      maxMessage = "La targa non deve avere più di {{ limit }} caratteri"
     * )
     */
    private $isaStealCarPlate;

    /**
     * @var string $isaStealCarModel
     *
     * @ORM\Column(name="isa_steal_car_model", type="string", length=60, nullable=false)
     */
    private $isaStealCarModel;

    /**
     * @var string $isaStealCarColor
     *
     * @ORM\Column(name="isa_steal_car_color", type="string", length=32, nullable=true)
     */
    private $isaStealCarColor;

    /**
     * @var string $isaStealDeviceSn
     *
     * @ORM\Column(name="isa_steal_device_sn", type="string", length=11, nullable=false)
     */
    private $isaStealDeviceSn;

    /**
     * @var IsaStealsStatus
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="IsaStealsStatus")
     * @ORM\JoinColumn(name="isa_steal_status", referencedColumnName="isa_steal_status_id", nullable=false, onDelete="cascade")
     */
    private $isaStealStatus;

    /**
     * @var IsaStealsTypes
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="IsaStealsTypes")
     * @ORM\JoinColumn(name="isa_steal_type", referencedColumnName="isa_steal_type_id", nullable=false, onDelete="cascade")
     */
    private $isaStealType;

    /**
     * @var datetime $isaStealInsertDate
     *
     * @ORM\Column(name="isa_steal_insert_date", type="datetime", nullable=false)
     */
    private $isaStealInsertDate;

    /**
     * @var datetime $isaStealDate
     *
     * @ORM\Column(name="isa_steal_date", type="datetime", nullable=false)
     */
    private $isaStealDate;

    /**
     * @var datetime $isaStealUpdateDate
     *
     * @ORM\Column(name="isa_steal_update_date", type="datetime", nullable=true)
     */
    private $isaStealUpdateDate;

    /**
     * @var string $isaStealHolderTel
     *
     * @ORM\Column(name="isa_steal_holder_tel", type="string", length=16, nullable=true)
     */
    private $isaStealHolderTel;

    /**
     * @var string $isaStealHolderName
     *
     * @ORM\Column(name="isa_steal_holder_name", type="string", length=50, nullable=true)
     */
    private $isaStealHolderName;

    /**
     * @var string $isaStealCarInsuranceNumber
     *
     * @ORM\Column(name="isa_steal_car_insurance_number", type="string", length=20, nullable=true)
     */
    private $isaStealCarInsuranceNumber;


    /**
     * @var string $isaStealLat
     *
     * @ORM\Column(name="isa_steal_lat", type="string", length=11, nullable=false)
     */
    private $isaStealLat;

    /**
     * @var string $isaStealLong
     *
     * @ORM\Column(name="isa_steal_long", type="string", length=11, nullable=false)
     */
    private $isaStealLong;

    /**
     * @var string $isaStealHandledLat
     *
     * @ORM\Column(name="isa_steal_handled_lat", type="string", length=11, nullable=true)
     */
    private $isaStealHandledLat;

    /**
     * @var string $isaStealHandledLong
     *
     * @ORM\Column(name="isa_steal_handled_long", type="string", length=11, nullable=true)
     */
    private $isaStealHandledLong;

    /**
     * @var string $isaStealNote
     *
     * @ORM\Column(name="isa_steal_note", type="string", length=256, nullable=true)
     */
    private $isaStealNote;

    /**
     * @var integer $isaStealTracking
     *
     * @ORM\Column(name="isa_steal_tracking", type="integer", length=2, nullable=true)
     */
    private $isaStealTracking;

    /**
     * @var string $isaStealComplaint
     *
     * @ORM\Column(name="isa_steal_complaint", type="string", length=32, nullable=true)
     */
    private $isaStealComplaint;

    /**
     * @var string $isaStealReverse
     *
     * @ORM\Column(name="isa_steal_reverse", type="string", length=128, nullable=true)
     */
    private $isaStealReverse;

    /**
     * @var string $isaStealUser
     *
     * @ORM\Column(name="isa_steal_user", type="string", length=30, nullable=true)
     */
    private $isaStealUser = NULL;
    
    /**
     * @var string $sendData
     *
     * @ORM\Column(name="send_data", type="integer", length=1, nullable=false)
     */
    private $sendData = 0;

    /*public function __construct()
    {
        $this->isaStealStatus = new ArrayCollection();
        $this->isaStealType = new ArrayCollection();
    }*/

    /**
     * @return string
     */
    public function getIsaStealNote()
    {
        return $this->isaStealNote;
    }

    /**
     * @param string $isaStealNote
     */
    public function setIsaStealNote($isaStealNote)
    {
        $this->isaStealNote = $isaStealNote;
    }

    /**
     * Get isaStealId
     *
     * @return integer
     */
    public function getIsaStealId()
    {
        return $this->isaStealId;
    }

    /**
     * Set isaStealCarPlate
     *
     * @param string $isaStealCarPlate
     *
     * @return IsaSteals
     */
    public function setIsaStealCarPlate($isaStealCarPlate)
    {
        $this->isaStealCarPlate = $isaStealCarPlate;

        return $this;
    }

    /**
     * Get isaStealCarPlate
     *
     * @return string
     */
    public function getIsaStealCarPlate()
    {
        return $this->isaStealCarPlate;
    }

    /**
     * Set isaStealCarModel
     *
     * @param string $isaStealCarModel
     *
     * @return IsaSteals
     */
    public function setIsaStealCarModel($isaStealCarModel)
    {
        $this->isaStealCarModel = $isaStealCarModel;

        return $this;
    }

    /**
     * Get isaStealCarModel
     *
     * @return string
     */
    public function getIsaStealCarModel()
    {
        return $this->isaStealCarModel;
    }

    /**
     * Set isaStealCarColor
     *
     * @param string $isaStealCarColor
     *
     * @return IsaSteals
     */
    public function setIsaStealCarColor($isaStealCarColor)
    {
        $this->isaStealCarColor = $isaStealCarColor;

        return $this;
    }

    /**
     * Get isaStealCarColor
     *
     * @return string
     */
    public function getIsaStealCarColor()
    {
        return $this->isaStealCarColor;
    }

    /**
     * Set isaStealDeviceSn
     *
     * @param string $isaStealDeviceSn
     *
     * @return IsaSteals
     */
    public function setIsaStealDeviceSn($isaStealDeviceSn)
    {
        $this->isaStealDeviceSn = $isaStealDeviceSn;

        return $this;
    }

    /**
     * Get isaStealDeviceSn
     *
     * @return string
     */
    public function getIsaStealDeviceSn()
    {
        return $this->isaStealDeviceSn;
    }

    /**
     * @return IsaStealsStatus
     */
    public function getIsaStealStatus()
    {
        return $this->isaStealStatus;
    }

    /**
     * @param IsaStealsStatus $isaStealStatus
     */
    public function setIsaStealStatus($isaStealStatus)
    {
        $this->isaStealStatus = $isaStealStatus;
    }

    /**
     * @return IsaStealsTypes
     */
    public function getIsaStealType()
    {
        return $this->isaStealType;
    }

    /**
     * @param IsaStealsTypes $isaStealType
     */
    public function setIsaStealType($isaStealType)
    {
        $this->isaStealType = $isaStealType;
    }

    /**
     * Set isaStealInsertDate
     *
     * @param \DateTime $isaStealInsertDate
     *
     * @return IsaSteals
     */
    public function setIsaStealInsertDate($isaStealInsertDate)
    {
        $this->isaStealInsertDate = $isaStealInsertDate;

        return $this;
    }

    /**
     * Get isaStealInsertDate
     *
     * @return \DateTime
     */
    public function getIsaStealInsertDate()
    {
        return $this->isaStealInsertDate;
    }

    /**
     * Set isaStealDate
     *
     * @param \DateTime $isaStealDate
     *
     * @return IsaSteals
     */
    public function setIsaStealDate($isaStealDate)
    {
        $this->isaStealDate = $isaStealDate;

        return $this;
    }

    /**
     * Get isaStealDate
     *
     * @return \DateTime
     */
    public function getIsaStealDate()
    {
        return $this->isaStealDate;
    }

    /**
     * Get isaStealUpdateDate
     *
     * @return \DateTime
     */
    public function getIsaStealUpdateDate()
    {
        return $this->isaStealUpdateDate;
    }

    /**
     * Set isaStealUpdateDate
     *
     * @param \DateTime $isaStealUpdateDate
     *
     * @return IsaSteals
     */
    public function setIsaStealUpdateDate($isaStealUpdateDate)
    {
        $this->isaStealUpdateDate = $isaStealUpdateDate;
    }

    /**
     * Set isaStealHolderTel
     *
     * @param string $isaStealHolderTel
     *
     * @return IsaSteals
     */
    public function setIsaStealHolderTel($isaStealHolderTel)
    {
        $this->isaStealHolderTel = $isaStealHolderTel;

        return $this;
    }

    /**
     * Get isaStealHolderTel
     *
     * @return string
     */
    public function getIsaStealHolderTel()
    {
        return $this->isaStealHolderTel;
    }

    /**
     * Set isaStealHolderName
     *
     * @param string $isaStealHolderName
     *
     * @return IsaSteals
     */
    public function setIsaStealHolderName($isaStealHolderName)
    {
        $this->isaStealHolderName = $isaStealHolderName;

        return $this;
    }

    /**
     * Get isaStealHolderName
     *
     * @return string
     */
    public function getIsaStealHolderName()
    {
        return $this->isaStealHolderName;
    }

    /**
     * Set isaStealCarInsuranceNumber
     *
     * @param string $isaStealCarInsuranceNumber
     *
     * @return IsaSteals
     */
    public function setIsaStealCarInsuranceNumber($isaStealCarInsuranceNumber)
    {
        $this->isaStealCarInsuranceNumber = $isaStealCarInsuranceNumber;

        return $this;
    }

    /**
     * Get isaStealCarInsuranceNumber
     *
     * @return string
     */
    public function getIsaStealCarInsuranceNumber()
    {
        return $this->isaStealCarInsuranceNumber;
    }

    /**
     * Set isaStealLat
     *
     * @param string $isaStealLat
     *
     * @return IsaSteals
     */
    public function setIsaStealLat($isaStealLat)
    {
        $this->isaStealLat = $isaStealLat;

        return $this;
    }

    /**
     * Get isaStealLat
     *
     * @return string
     */
    public function getIsaStealLat()
    {
        return $this->isaStealLat;
    }

    /**
     * Set isaStealLong
     *
     * @param string $isaStealLong
     *
     * @return IsaSteals
     */
    public function setIsaStealLong($isaStealLong)
    {
        $this->isaStealLong = $isaStealLong;

        return $this;
    }

    /**
     * Get isaStealLong
     *
     * @return string
     */
    public function getIsaStealLong()
    {
        return $this->isaStealLong;
    }

    /**
     * Set isaStealHandledLat
     *
     * @param string $isaStealHandledLat
     *
     * @return IsaSteals
     */
    public function setIsaStealHandledLat($isaStealHandledLat)
    {
        $this->isaStealHandledLat = $isaStealHandledLat;

        return $this;
    }

    /**
     * Get isaStealHandledLat
     *
     * @return string
     */
    public function getIsaStealHandledLat()
    {
        return $this->isaStealHandledLat;
    }

    /**
     * Set isaStealHandledLong
     *
     * @param string $isaStealHandledLong
     *
     * @return IsaSteals
     */
    public function setIsaStealHandledLong($isaStealHandledLong)
    {
        $this->isaStealHandledLong = $isaStealHandledLong;

        return $this;
    }

    /**
     * Get isaStealHandledLong
     *
     * @return string
     */
    public function getIsaStealHandledLong()
    {
        return $this->isaStealHandledLong;
    }

    /**
     * Get isaStealTracking
     *
     * @return integer
     */
    public function getIsaStealTracking()
    {
        return $this->isaStealTracking;
    }

    /**
     * Set isaStealTracking
     *
     * @param integer $isaStealTracking
     *
     * @return IsaSteals
     */
    public function setIsaStealTracking($isaStealTracking)
    {
        $this->isaStealTracking = $isaStealTracking;
    }

    /**
     * @return string
     */
    public function getIsaStealComplaint()
    {
        return $this->isaStealComplaint;
    }

    /**
     * @param $isaStealComplaint
     * @return $this
     */
    public function setIsaStealComplaint($isaStealComplaint)
    {
        $this->isaStealComplaint = $isaStealComplaint;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsaStealReverse()
    {
        return $this->isaStealReverse;
    }

    /**
     * @param $isaStealReverse
     * @return $this
     */
    public function setIsaStealReverse($isaStealReverse)
    {
        $this->isaStealReverse = $isaStealReverse;
        return $this;
    }

    /**
     * Set isaStealUser
     *
     * @param string $isaStealUser
     *
     * @return IsaSteals
     */
    public function setIsaStealUser($isaStealUser)
    {
        $this->isaStealUser = $isaStealUser;

        return $this;
    }

    /**
     * Get isaStealUser
     *
     * @return string
     */
    public function getIsaStealUser()
    {
        return $this->isaStealUser;
    }

    public static function isaStealLabelStatus($status)
    {
        switch($status) {
            case 0:
                return "Da lavorare";
            case 1:
                return "In lavorazione";
            case 2:
                return "Chiuso";
            case 3:
                return "In attesa";
            case 4:
                return "Chiuso e confermato";
            default:
                return "---";
        }
    }
    
    public function getSendData() {
        return $this->sendData;
    }

    public function setSendData($sendData) {
        $this->sendData = $sendData;
    }

    public static function isaStealLabelTracking($status) {
        switch ($status) {
            case 0:
                return "Disattivo";
            case 1:
                return "Attivo";
            case 2:
                return "In disattivazione";
            case 3:
                return "In attivazione";
        }
    }

    /*public function __toString()
    {
        return (string) $this->getTicket();
    }*/

}
