<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user", indexes={
 *  @ORM\Index(name="type_idx", columns={"user_type"}),
 *  @ORM\Index(name="emailsent_idx", columns={"email_sent"})
 * })
 * @UniqueEntity(fields={"email"}, message="Username giá esistente")
 */
class User extends BaseUser
{
    const ROOT = 1;
    const FURTI = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", name="user_type")
     */
    protected $userType = null;

    
    /**
     * @ORM\Column(type="boolean", name="email_sent")
     */
    protected $emailSent = false;
    
    /**
     * @ORM\Column(type="string", name="recover")
     */
    protected $recover;      
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->userType = self::ROOT;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getEmailSent() {
        return $this->emailSent;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEmailSent($emailSent) {
        $this->emailSent = $emailSent;
    }

    public function getRecover() {
        return $this->recover;
    }

    public function setRecover($recover) {
        $this->recover = $recover;
    }
    
    public function getUserType() {
        return $this->userType;
    }

    public function setUserType($userType) {
        $this->userType = $userType;
    }    
    
}