<?php
// src/AppBundle/Entity/IsaStealsStatus.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\IsaStealsStatus
 *
 * @ORM\Table(name="isa_steals_status")
 * @ORM\Entity
 *
 */
class IsaStealsStatus
{
    /**
     * @var integer $isaStealStatusId
     *
     * @ORM\Column(name="isa_steal_status_id", type="integer", length=20)
     * @ORM\Id
     */
    private $isaStealStatusId;

    /**
     * @var string $isaStealStatusDescription
     *
     * @ORM\Column(name="isa_steal_status_description", type="string", length=256, nullable=false)
     */
    private $isaStealStatusDescription = NULL;

    /**
     * @return int
     */
    public function getIsaStealStatusId()
    {
        return $this->isaStealStatusId;
    }

    /**
     * @param int $isaStealStatusId
     */
    public function setIsaStealStatusId($isaStealStatusId)
    {
        $this->isaStealStatusId = $isaStealStatusId;
    }

    /**
     * @return string
     */
    public function getIsaStealStatusDescription()
    {
        return $this->isaStealStatusDescription;
    }

    /**
     * @param string $isaStealStatusDescription
     */
    public function setIsaStealStatusDescription($isaStealStatusDescription)
    {
        $this->isaStealStatusDescription = $isaStealStatusDescription;
    }

    function __toString()
    {
        return $this->isaStealStatusDescription;
    }


}
