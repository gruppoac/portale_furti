<?php
// src/AppBundle/Entity/IsaStealsLog.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

/**
 * AppBundle\Entity\IsaStealsLog
 *
 * @ORM\Table(name="isa_steals_log", indexes={
 *      @ORM\Index(name="steal_id_idx", columns={"isa_steal_id"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\IsaStealsLogRepository")
 * @UniqueEntity("isa_steal_log_id")
 */
class IsaStealsLog
{
    /**
     * @var integer $isaStealLogId
     *
     * @ORM\Column(name="isa_steal_log_id", type="bigint", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $isaStealLogId;

    /**
     * @var IsaSteals
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="IsaSteals")
     * @ORM\JoinColumn(name="isa_steal_id", referencedColumnName="isa_steal_id", nullable=true)
     * @Type("AppBundle\Entity\IsaSteals")
     */
    private $isaStealId;

    /**
     * @var datetime $isaStealLogInsertDate
     * @ORM\Column(name="isa_steal_log_insert_date", type="datetime", nullable=false)
     */
    private $isaStealLogInsertDate;

    /**
     * @var IsaStealsLogTypes
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="IsaStealsLogTypes")
     * @ORM\JoinColumn(name="isa_steal_log_type", referencedColumnName="isa_steal_log_type_id", nullable=false)
     * @Type("AppBundle\Entity\IsaStealsLogTypes")
     */
    private $isaStealLogType;

    /**
     * @var string $isaStealLogNote
     *
     * @ORM\Column(name="isa_steal_log_note", type="text", nullable=true)
     */
    private $isaStealLogNote;

    /**
     * @var string $isaStealLogUser
     *
     * @ORM\Column(name="isa_steal_log_user", type="string", length=30, nullable=true)
     */
    private $isaStealLogUser = NULL;

    /**
     * @return int
     */
    public function getIsaStealLogId()
    {
        return $this->isaStealLogId;
    }

    /**
     * @param int $isaStealLogId
     */
    public function setIsaStealLogId($isaStealLogId)
    {
        $this->isaStealLogId = $isaStealLogId;
    }

    /**
     * @return int
     */
    public function getIsaStealId()
    {
        return $this->isaStealId;
    }

    /**
     * @param int $isaStealId
     */
    public function setIsaStealId($isaStealId)
    {
        $this->isaStealId = $isaStealId;
    }

    /**
     * @return datetime
     */
    public function getIsaStealLogInsertDate()
    {
        return $this->isaStealLogInsertDate;
    }

    /**
     * @param datetime $isaStealLogInsertDate
     */
    public function setIsaStealLogInsertDate($isaStealLogInsertDate)
    {
        $this->isaStealLogInsertDate = $isaStealLogInsertDate;
    }

    /**
     * @return string
     */
    public function getIsaStealLogType()
    {
        return $this->isaStealLogType;
    }

    /**
     * @param string $isaStealLogType
     */
    public function setIsaStealLogType($isaStealLogType)
    {
        $this->isaStealLogType = $isaStealLogType;
    }

    /**
     * Get isaStealLogUser
     *
     * @return string
     */
    public function getIsaStealLogUser()
    {
        return $this->isaStealLogUser;
    }

    /**
     * Set isaStealLogUser
     *
     * @param string $isaStealLogUser
     *
     * @return IsaStealsLog
     */
    public function setIsaStealLogUser($isaStealLogUser)
    {
        $this->isaStealLogUser = $isaStealLogUser;
    }

    /**
     * Get isaStealLogNote
     *
     * @return string
     */
    public function getIsaStealLogNote()
    {
        return $this->isaStealLogNote;
    }

    /**
     * Set isaStealLogNote
     *
     * @param string $isaStealLogNote
     *
     * @return IsaStealsLog
     */
    public function setIsaStealLogNote($isaStealLogNote)
    {
        $this->isaStealLogNote = $isaStealLogNote;
    }


}
