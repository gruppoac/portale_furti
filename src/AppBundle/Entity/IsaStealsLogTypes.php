<?php
// src/AppBundle/Entity/IsaStealsLogTypes.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\IsaStealsLogTypes
 *
 * @ORM\Table(name="isa_steals_log_types")
 * @ORM\Entity
 *
 */
class IsaStealsLogTypes
{
    /**
     * @var integer $isaStealLogTypeId
     *
     * @ORM\Column(name="isa_steal_log_type_id", type="bigint", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $isaStealLogTypeId;

    /**
     * @var string $isaStealLogTypeDescription
     *
     * @ORM\Column(name="isa_steal_log_type_description", type="string", length=256, nullable=true)
     */
    private $isaStealLogTypeDescription = NULL;



    /**
     * Get isaStealLogTypeId
     *
     * @return integer
     */
    public function getIsaStealLogTypeId()
    {
        return $this->isaStealLogTypeId;
    }

    /**
     * Set isaStealLogTypeDescription
     *
     * @param string $isaStealLogTypeDescription
     *
     * @return IsaStealsLogTypes
     */
    public function setLogTypeDescription($isaStealLogTypeDescription)
    {
        $this->isaStealLogTypeDescription = $isaStealLogTypeDescription;

        return $this;
    }

    /**
     * Get isaStealLogTypeDescription
     *
     * @return string
     */
    public function getIsaStealLogTypeDescription()
    {
        return $this->isaStealLogTypeDescription;
    }

    function __toString()
    {
        return $this->isaStealLogTypeDescription;
    }


}
