<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\IsaSteals;
use Doctrine\ORM\EntityRepository;
use AppBundle\Model\Filter\FilterModel;
use Doctrine\ORM\QueryBuilder;

class IsaStealsRepository extends EntityRepository
{
    /**
     * Prende l'elenco di record avente un oggetto Filter come input
     * @todo spostare in un trait?
     * @param FilterModel $filter
     * @return IsaSteals[]
     */
    public function getRecordsByFilter(FilterModel $filter, $status = null) {
        $qb = $this->getFilterQb($filter);
        if ($status) {
            $qb->andWhere("s.isaStealStatus IN(:status)")->setParameter('status', $status );
        }
        //applica ordinamento e paginazione
        $sort = $filter->getSort();
        if($sort->getOrder() ) {
            $qb->orderBy( $sort->getOrder(), $sort->getType() );
        }
        $qb->setFirstResult( ($filter->getPage() -1 ) * $filter->getLimit() )->setMaxResults( $filter->getLimit() );
        //risultati
        $results = $qb->getQuery()->getResult();
        return $results;
    }

    /**
     * Contatore record dato un determinato filtro
     * @todo spsotare in trait?
     * @param FilterModel $filter
     * @return integer
     */
    public function getCountByFilter(FilterModel $filter, $status = null) {
        $qb = $this->getFilterQb($filter);
        if ($status) {
            $qb->andWhere("s.isaStealStatus IN(:status)")->setParameter('status', $status );
        }
        $qb->select('COUNT(s)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Applica filtri al querybuilder
     * @param QueryBuilder $qb
     * @param FilterModel $filter
     */
    public function getFilterCriteria(QueryBuilder $qb, FilterModel $filter) {
        if($filter->getQuery()) {
            $qb->andWhere('s.isaStealCarPlate LIKE :query')->setParameter('query', "%".$filter->getQuery()."%");
        }
        if ($filter->getDateRange()) {
            $start = (null !== $filter->getDateRange()->getStart()) ? $filter->getDateRange()->getStart()->format('Y-m-d') : null;
            $end = (null !== $filter->getDateRange()->getEnd()) ? $filter->getDateRange()->getEnd()->format('Y-m-d') : null;
            if ($start || $end) {
                if ($start & $end) {
                    $qb->andWhere('s.isaStealInsertDate BETWEEN :start and :end')
                        ->setParameters(['start' => $start, 'end' => $end]);
                }
                else if ($start) {
                    $qb->andWhere('s.isaStealInsertDate >= :start')
                        ->setParameter('start', $start);
                } else {
                    $qb->andWhere('s.isaStealInsertDate <= :end')
                        ->setParameter('end', $end);
                }
            }

        }
    }

    /**
     * Inizializza il querybuilder con i filtri in input
     * @param FilterModel $filter
     * @return QueryBuilder
     */
    public function getFilterQb(FilterModel $filter) {
        $qb = $this->createQueryBuilder('s');
        $this->getFilterCriteria($qb, $filter);
        return $qb;
    }
}