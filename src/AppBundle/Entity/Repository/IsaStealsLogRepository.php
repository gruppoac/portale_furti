<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\IsaStealsLog;
use Doctrine\ORM\EntityRepository;
use AppBundle\Model\Filter\FilterModel;
use Doctrine\ORM\QueryBuilder;

class IsaStealsLogRepository extends EntityRepository
{
    /**
     * Prende l'elenco di record avente un oggetto Filter come input
     * @todo spostare in un trait?
     * @param FilterModel $filter
     * @return IsaStealsLog[]
     */
    public function getRecordsByFilter(FilterModel $filter) {
        $qb = $this->getFilterQb($filter);
        //applica ordinamento e paginazione
        $sort = $filter->getSort();
        if($sort->getOrder() ) {
            $qb->orderBy( $sort->getOrder(), $sort->getType() );
        } else {
            $qb->orderBy('sl.isaStealLogId', 'DESC');
        }
        $qb->setFirstResult( ($filter->getPage() -1 ) * $filter->getLimit() )->setMaxResults( $filter->getLimit() );
        //risultati
        $results = $qb->getQuery()->getResult();
        return $results;
    }

    /**
     * Contatore record dato un determinato filtro
     * @todo spsotare in trait?
     * @param FilterModel $filter
     * @return integer
     */
    public function getCountByFilter(FilterModel $filter) {
        $qb = $this->getFilterQb($filter);
        $qb->select('COUNT(sl)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Applica filtri al querybuilder
     * @param QueryBuilder $qb
     * @param FilterModel $filter
     */
    public function getFilterCriteria(QueryBuilder $qb, FilterModel $filter) {
        if($filter->getQuery()) {
            $qb->andWhere('sl.isaStealLogUser LIKE :query')->setParameter('query', "%".$filter->getQuery()."%");
        }
        if ($filter->getDateRange()) {
            $start = (null !== $filter->getDateRange()->getStart()) ? $filter->getDateRange()->getStart()->setTime('00','00','00') : null;
            $end = (null !== $filter->getDateRange()->getEnd()) ? $filter->getDateRange()->getEnd()->setTime('23','59','59') : null;
            if ($start || $end) {
                if ($start && $end) {
                    $qb->andWhere('sl.isaStealLogInsertDate BETWEEN :start and :end')
                        ->setParameter('start', $start->format('Y-m-d H:i:s'))
                        ->setParameter('end', $end->format('Y-m-d H:i:s'));
                }
                else if ($start) {
                    $qb->andWhere('sl.isaStealLogInsertDate >= :start')
                        ->setParameter('start', $start->format('Y-m-d H:i:s'));
                } else {
                    $qb->andWhere('sl.isaStealLogInsertDate <= :end')
                        ->setParameter('end', $end->format('Y-m-d H:i:s'));
                }
            }

        }
    }

    /**
     * Inizializza il querybuilder con i filtri in input
     * @param FilterModel $filter
     * @return QueryBuilder
     */
    public function getFilterQb(FilterModel $filter) {
        $qb = $this->createQueryBuilder('sl');
        $this->getFilterCriteria($qb, $filter);
        return $qb;
    }
}