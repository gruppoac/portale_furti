<?php

namespace AppBundle\Entity;

use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

/**
 * @ORM\Table(name="geos", indexes={
 *      @ORM\Index(name="point_idx", columns={"point"}),
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\GeoRepository")
 */
class Geo {
  
    /**
     * Chiave primaria
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="label", type="string", nullable=false)
     * @Assert\NotBlank(message="query mancante")
     */
    protected $query;

    /**
     * @var Point
     * @ORM\Column(name="point", type="point", nullable=true)
     * @Assert\NotBlank(message="Coordinate mancanti")
     * @Assert\Valid
     */
    protected $point;
        
    /**
     * @ORM\Column(name="indirizzo", type="string", nullable=true)
     * @Assert\NotBlank(message="Indirizzo assente")
     */
    protected $indirizzo;
    /**
     * @ORM\Column(name="regione", type="string", nullable=true)
     * @Assert\NotBlank(message="Regione assente")
     */
    protected $regione;

    /**
     * @ORM\Column(name="provincia", type="string", nullable=true)
     * @Assert\NotBlank(message="provincia assente")
     */
    protected $provincia;

    /**
     * @ORM\Column(name="provincia_code", type="string", nullable=true)
     * @Assert\NotBlank(message="PR assente")
     */
    protected $provinciaCode;
    
    /**
     * @ORM\Column(name="comune", type="string", nullable=true)
     * @Assert\NotBlank(message="comune assente")
     */
    protected $comune;
   
    /**
     * @ORM\Column(name="cap", type="string", nullable=true)
     * @Assert\NotBlank(message="CAP assente")
     */
    protected $cap;

    public function getId() {
        return $this->id;
    }

    public function getQuery() {
        return $this->query;
    }

    public function getPoint() {
        return $this->point;
    }

    public function getIndirizzo() {
        return $this->indirizzo;
    }

    public function getRegione() {
        return $this->regione;
    }

    public function getProvincia() {
        return $this->provincia;
    }

    public function getProvinciaCode() {
        return $this->provinciaCode;
    }

    public function getComune() {
        return $this->comune;
    }

    public function getCap() {
        return $this->cap;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setQuery($query) {
        $this->query = $query;
        return $this;
    }

    public function setPoint(Point $point) {
        $this->point = $point;
        return $this;
    }

    public function setIndirizzo($indirizzo) {
        $this->indirizzo = $indirizzo;
        return $this;
    }

    public function setRegione($regione) {
        $this->regione = $regione;
        return $this;
    }

    public function setProvincia($provincia) {
        $this->provincia = $provincia;
        return $this;
    }

    public function setProvinciaCode($provinciaCode) {
        $this->provinciaCode = $provinciaCode;
        return $this;
    }

    public function setComune($comune) {
        $this->comune = $comune;
        return $this;
    }

    public function setCap($cap) {
        $this->cap = $cap;
        return $this;
    }
    
}
